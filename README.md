# MMenard Typescript/React/Redux Demo

Typescript port of my [React/Redux/ES6 demo](https://bitbucket.org/MelanieMenard/mmenard-demo).

Demo app fetching pictures related to a predefined list of Artists, thinkers and trailblazers heritage sites from Flickr API. 

On the homepage, view the 20 most popular images for each location, or filter images by location. Using D3, show relative popularity of each location based on total matching images on the server, both on a tag cloud and a geo-mapped animated bubble chart. The bubble chart animates when data is fetched for each location, and when a location is selected via the tag cloud.

If you filter on a location that has more than 20 images, a 'View more' link takes you (via react-router) to a different page with a lazy-loaded paginated list of the remaining images for the location.

Navigate to detail page for individual images with react-router and fetch extra image detail using a different API call.

Bootstrapped with my fork of create-react-app modified to enable custom eslint config and absolute import paths:
[https://github.com/MelanieMenard/create-react-app/commits/mmenard-custom-scripts](https://github.com/MelanieMenard/create-react-app/commits/mmenard-custom-scripts)

Deployed to Heroku with zero-configuration using [Heroku Buildpack for create-react-app](https://github.com/mars/create-react-app-buildpack).

Uses Typescript, React, React-Router, Redux, Redux thunk, Axios, custom SASS, D3 and some material UI components. Webpack and TSlint in build tools. Jest and Enzyme for testing.

### Contribution guidelines ###

This is a demo for the sole purpose of demonstrating my coding skills. Thank you.

### Who do I talk to? ###

contact@melaniemenard.com

### How do I get set up? ###

1 - Run the app locally:

```
cd mmenard-demo-typescript
npm install
npm start
```

2 - In another tab

```
npm test
```

Open http://localhost:3000 to view it in the browser. The app will reload when there are changes to the code.

2 - View the app on Heroku: [https://mmenard-demo-typescript.herokuapp.com/](https://mmenard-demo-typescript.herokuapp.com/).