const path = require('path');

module.exports = {
  'rules': {
    // always put parentheses around arguments in arrow functions as it is more readable
    'arrow-parens': ['error', 'always'],
    // put else on line AFTER the closing } of preceding if, we find it more readable PLUS it allows inserting comment lines between if and else !!!
    // https://eslint.org/docs/rules/brace-style
    'brace-style': ['error', 'stroustrup', { allowSingleLine: true }],
    // we see no good reason to enforce this rule
    // https://eslint.org/docs/rules/class-methods-use-this#enforce-that-class-methods-utilize-this-class-methods-use-this
    'class-methods-use-this': 'off',
    // named exports are preferred to optimise webpack tree shaking
    'import/prefer-default-export': 'off',
    'import/no-default-export': 'error',
    // we write code on large screens, it's more readable to keep statements on 1 line that arbitrarily restrict line length 
    'max-len': 'off',
    // console logs are useful to help us debug recent code on dev. a function disables all alerts in production builds
    'no-console': 'off',
    // we reassign classes to use connect on components when we don't want to rename them
    'no-class-assign': 'off',
    // this rules imposes unnecesary parentheses because it ignores basic arithmetic operator priority
    'no-mixed-operators': 'off',
    // feel free to use 2 empty lines to separate groups of functions that go together within a file, for example request/success/error groups
    'no-multiple-empty-lines': 'off',
    // we allow param reassign to set defaults inside functions
    // https://blog.javascripting.com/2015/09/07/fine-tuning-airbnbs-eslint-config/
    'no-param-reassign': 'off',
    // allow ++ in the final expression of for loop
    'no-plusplus': ['error', { "allowForLoopAfterthoughts": true }],
    // turn this rule off if you will never use an object that shadows an Object.prototype method or which does not inherit from Object.prototype
    // https://eslint.org/docs/rules/no-prototype-builtins#disallow-use-of-objectprototypes-builtins-directly-no-prototype-builtins
    // this imposes needlessly verbose syntax when the real issue is you should neveer overwrite standard JS prototypes
    'no-prototype-builtins': 'off',
    // we find ternary more readable than the suggested airbnb alternative
    'no-unneeded-ternary': 'off',
    // unused var changed to warning (as functions may be made to use later only)
    // and allowed in arguments as you may have standard arguments to conform to a pattern such as (resolve, reject)
    // but warning is useful to help us get rid af legacy vars no longer used.
    'no-unused-vars': ['warn', {"vars": "local", "args": "none"}],
    // again feels free to use white space for readability and separating logical blocks of code
    'padded-blocks': 'off',
    // if we get several things out of same object, we prefer destructuring to save repetition
    // however we see no good reason to enforce destructuring to get 1 thing out of an object, it's needlessly complicated syntax
    // and array destructuring syntax is just confusing and unreadable for no actual gain
    // https://eslint.org/docs/rules/prefer-destructuring
    'prefer-destructuring': ['off', {
      VariableDeclarator: {
        array: false,
        object: true,
      },
      AssignmentExpression: {
        array: false,
        object: true,
      },
    }, {
      enforceForRenamedProperties: false,
    }],
    // same as above for react props, use whichever is more readable dependeing on how many props you access
    'react/destructuring-assignment': ['off', 'always'],
    // explicitely specifiying the radix was only necessary for a bug in IE8
    // https://blog.javascripting.com/2015/09/07/fine-tuning-airbnbs-eslint-config/
    'radix': 'off',
    // allow jsx in .js files because jsx is not a language, just an extension of js
    'react/jsx-filename-extension': 'off',
    // I prefer React.Fragment to the shorthand as it's more eplicit than an empty tag
    'react/jsx-fragments': 'off',
    // we allow multiple components in same file when they afre always used together, for examples list elements and list
    'react/no-multi-comp': 'off',
    // Ignore className for propType!
    'react/prop-types': ['error', {
      ignore: ['className'],
      customValidators: [],
      skipUndeclared: false
    }],
    // do not require default props because we make the default intelligently in code
    'react/require-default-props': ['off', {
      forbidDefaultForRequired: true,
    }],
  },
  'extends': 'airbnb',
  'settings': {
  // enables eslint to find the imports that use webpack config resolve alias paths, without this eslint thinks they are unresolved imports
    'import/resolver':  {
      alias: {
        map: [
         ['AppSrc', path.resolve(__dirname, './src')],
        ]
      },
      'webpack': {
        'config': './node_modules/mmenard-react-scripts/config/webpack.config.js'
      }
    }
  },
  // list here all the global variables defined by webpack.ProvidePlugin and webpack.DefinePlugin, otherwise eslint thinks they are undefined
  // you also need to tell some default JS global variable document and window, even though you'd expect a JS linter to know that!
  'globals': {
    document: true,
    window: true,
  }
}
