/* ------------------------------------------- */
/*   Error handler
/*   Flickr API error spec: https://www.flickr.com/services/api/flickr.photos.search.html
/* ------------------------------------------- */

import { FormattedError } from 'AppSrc/core/interfaces/Error';

// handle server errors with response codes other than 200 properly identified as errors
const handleError = (error: any) => {

  const formattedError: FormattedError = {
    code: 0,
    message: 'Error fetching media from server.',
  };

  // if error hidden in a response with status 200
  if (error.code) {
    formattedError.code = error.code;
    if (error.message) {
      formattedError.message = error.message;
    }
  }
  // proper API error
  else if (error.response && error.response.code) {
    formattedError.code = error.response.code;
    if (error.response.message) {
      formattedError.message = error.response.message;
    }
  }

  // throw the error so it gets caught in redux thunk action
  // throw formattedError, formattedError;
  console.log('handleError', formattedError);
  return Promise.reject(formattedError);
};


// catch errors hidden in a response with status 200
const catchHiddenError = (response: any) => {

  if (response.data.stat === 'fail' && response.data.code) {

    console.log('catchHiddenError', response.data);
    // throw an error that the regular error handler will catch
    // throw response.data;
    return Promise.reject(response.data);
  }

  // return a resolved promise (the status 200 is genuine success)
  return Promise.resolve(response);
  // return response;
};


export {
  catchHiddenError,
  handleError,
};
