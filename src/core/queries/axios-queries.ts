/* ------------------------------------------- */
/*   Axios instance that reused accross queries for efficiency
/*   Generic REST query functions making use of the instance, used in the async thunk actions
/* ------------------------------------------- */

import axios from 'axios';
import http from 'http';
import https from 'https';
import { AppCredentials } from 'AppSrc/core/auth/auth';
import {
  catchHiddenError,
  handleError,
} from 'AppSrc/core/queries/error-handler';
import {
  MediaDetailInterface,
  MediaItem,
  FlickrPhotoSearchResponse,
} from 'AppSrc/core/interfaces/Media';


const MEDIA_PER_PAGE = 20;


// First you set global config defaults that will be applied to every instances and requests
// https://github.com/axios/axios#global-axios-defaults
// 50 sec timeout
axios.defaults.timeout = 50000;
// follow up to 10 HTTP 3xx redirects
axios.defaults.maxRedirects = 10;
// cap the maximum content length we'll accept to 50MBs, just in case
axios.defaults.maxContentLength = 50 * 1000 * 1000;
// keepAlive pools and reuses TCP connections, so it's faster
axios.defaults.httpAgent = new http.Agent({ keepAlive: true });
axios.defaults.httpsAgent = new https.Agent({ keepAlive: true });


// then you create instance specific config when creating the instance
// https://github.com/axios/axios#custom-instance-defaults
// if we had authorization headers we would set them here
const axiosInstance = axios.create();


// utility function for posting a REST GET query to axios
// includes handling of 200 responses with hidden errors
// and standard error responses
// returns a promise with the response
// for REST queries, the endpoint depends on the data, so getRESTQuery gets it as an argument along with the query
// so this utility function is reusable for any REST data
const getRESTQuery = (endpoint: string, query: any) => axiosInstance.get(endpoint, { params: query })
  .then((response) => catchHiddenError(response))
  .catch((error) => handleError(error));


interface FlickrPhotoSearchQuery {
  page: number;
  method: string;
  api_key: string;
  tags: string;
  tag_mode: string;
  sort: string;
  per_page: number;
  format: string;
  nojsoncallback: string;
}

interface FlickrRawMediaItem {
  id: string;
  title: string;
  secret: string;
  owner: string;
  farm: string;
  server: string;
}

// GET query to Flickr Photo Search API via axios
// returns a promise with the response
const getFlickrSearchQuery = (searchString: string, page: number = 1, perPage: number = MEDIA_PER_PAGE): Promise<FlickrPhotoSearchResponse> => {

  // Flickr Photo Search API url
  // https://www.flickr.com/services/api/flickr.photos.search.html
  const endpoint = 'https://api.flickr.com/services/rest/';

  const formattedQueryTerms = searchString.split(' ').join(',');
  // nojsoncallback necessary to make the API return plain JSON rather than JSONP
  const query: FlickrPhotoSearchQuery = {
    page,
    method: 'flickr.photos.search',
    api_key: String(AppCredentials.Flickr.key),
    tags: formattedQueryTerms,
    // tags: searchString,
    tag_mode: 'all',
    sort: 'interestingness-desc',
    per_page: perPage,
    format: 'json',
    nojsoncallback: 'true',
  };

  return getRESTQuery(endpoint, query)
    .then((response) => {

      // work out the media url from Flickr search info
      // https://www.flickr.com/services/api/misc.urls.html
      const formattedItems: MediaItem[] = response.data.photos.photo.map((item: FlickrRawMediaItem) => {
        const formattedResult: MediaItem = {
          title: item.title,
          id: item.id,
          secret: item.secret,
          authorId: item.owner,
          image: `https://farm${item.farm}.staticflickr.com/${item.server}/${item.id}_${item.secret}_z.jpg`,
        };
        return formattedResult;
      });

      const formattedResponse: FlickrPhotoSearchResponse = {
        total: response.data.photos.total,
        pages: response.data.photos.pages,
        page: response.data.photos.page,
        perPage: response.data.photos.perpage,
        items: formattedItems,
      };

      return formattedResponse;
    });
};


interface FlickrPhotoQuery {
  method: string;
  api_key: string;
  photo_id: string;
  format: string;
  nojsoncallback: string;
}

// GET query to Flickr Photo Info API via axios
// returns a promise with the response
const getFlickrPhotoInfoQuery = (photoId: string): Promise<MediaDetailInterface> => {

  // Flickr Photo Info API url
  // https://www.flickr.com/services/api/flickr.photos.getInfo.html
  const endpoint = 'https://api.flickr.com/services/rest/';

  // nojsoncallback necessary to make the API return plain JSON rather than JSONP
  const query: FlickrPhotoQuery = {
    method: 'flickr.photos.getInfo',
    api_key: String(AppCredentials.Flickr.key),
    photo_id: photoId,
    format: 'json',
    nojsoncallback: 'true',
  };

  return getRESTQuery(endpoint, query)
    .then((response) => {

      const photoData = response.data.photo;
      // flickr starts its key name by underscores!
      /* eslint-disable no-underscore-dangle */
      const formattedPhotoData: MediaDetailInterface = {
        id: photoData.id,
        secret: photoData.secret,
        link: `https://farm${photoData.farm}.staticflickr.com/${photoData.server}/${photoData.id}_${photoData.secret}_b.jpg`,
        title: photoData.title._content,
        description: photoData.description._content,
        author: photoData.owner.username,
        dateTaken: photoData.dates.taken,
      };
      return formattedPhotoData;
    });
};


export {
  axiosInstance,
  MEDIA_PER_PAGE,
  getFlickrSearchQuery,
  getFlickrPhotoInfoQuery,
};

