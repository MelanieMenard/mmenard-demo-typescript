import React from 'react';
import { rendersWithoutCrashing, matchesThePreviousSnapshot } from 'AppSrc/core/test/utils/component-test-utils';
import { App } from './app';

jest.mock('AppSrc/core/components/app-layout/app-layout', () => (
  { AppLayout: 'mock-app-layout' }
));

describe('App', () => {
  rendersWithoutCrashing(<App />);
  // Match against a shallow snapshot.
  matchesThePreviousSnapshot(<App />, {shallow: true});
});
