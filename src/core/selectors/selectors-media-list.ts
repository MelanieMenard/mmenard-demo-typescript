/* ----------------------------------------------- */
/*   Reusable selectors to use in mapStateToProps
/*   so that the props only change when the actual bit of state they depend on changes
/*   https://redux.js.org/recipes/computing-derived-data
/*   https://redux-starter-kit.js.org/api/createselector
/*   https://github.com/reduxjs/reselect
/* ----------------------------------------------- */


import { createDeepEqualSelector } from 'AppSrc/core/selectors/create-deep-equal-selector';
import { selectedTagIdSelector } from 'AppSrc/core/selectors/selectors-tags';
import { MEDIA_PER_PAGE } from 'AppSrc/core/queries/axios-queries';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { MediaItem } from 'AppSrc/core/interfaces/Media';


// MM: it seems you don't have the pass the whole props as arguments
// here we let connect pass just locationId, as it sometimes comes on its own, sometimes from router match
// https://blog.isquaredsoftware.com/2017/12/idiomatic-redux-using-reselect-selectors/
const locationIdPropSelector = (state: AppState, locationId: string) => (locationId);

const mediaByLocationSelector = (state: AppState) => (state.mediaList.mediaByLocation);


// MM: 'makeSelector' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components

const makeMediaByLocationIdItems = () => (createDeepEqualSelector(
  [mediaByLocationSelector, locationIdPropSelector],
  (mediaByLocation, locationId) => (mediaByLocation[locationId].items),
));

const makeMediaByLocationIdIsFetching = () => (createDeepEqualSelector(
  [mediaByLocationSelector, locationIdPropSelector],
  (mediaByLocation, locationId) => (mediaByLocation[locationId].isFetching),
));

const makeMediaByLocationIdIsFetched = () => (createDeepEqualSelector(
  [mediaByLocationSelector, locationIdPropSelector],
  (mediaByLocation, locationId) => (mediaByLocation[locationId].isFetched),
));

const makeMediaByLocationIdFullyLoaded = () => (createDeepEqualSelector(
  [mediaByLocationSelector, locationIdPropSelector],
  (mediaByLocation, locationId) => {

    const mediaForLocation = mediaByLocation[locationId];
    const currentPage = mediaForLocation.currentPage;
    const totalPages = mediaForLocation.totalPages;
    // this tells the lazyloader to fetch the next page
    // if the value is the same as the previous time (i.e. fully loaded) the lazy loader won't fire a request
    const fullyLoaded = (totalPages > 0) && (currentPage === totalPages);
    return fullyLoaded;
  },
));

const makeMediaByLocationIdLazyLoadParams = () => (createDeepEqualSelector(
  [mediaByLocationSelector, locationIdPropSelector],
  (mediaByLocation, locationId) => {

    const mediaForLocation = mediaByLocation[locationId];
    const isFetching = mediaForLocation.isFetching;
    const currentPage = mediaForLocation.currentPage;
    const totalPages = mediaForLocation.totalPages;
    const fetchNextPage = (currentPage < totalPages) ? currentPage + 1 : currentPage;

    const lazyLoadParams = {
      isFetching,
      paginationData: {
        page: fetchNextPage,
        perPage: MEDIA_PER_PAGE,
      },
      params: {
        locationId,
      },
    };

    return lazyLoadParams;
  },
));

const mediaByLocationArraySelector = createDeepEqualSelector(
  [mediaByLocationSelector],
  (mediaByLocation) => {

    const mediaByLocationArray = Object.values(mediaByLocation);
    return mediaByLocationArray;
  },
);

const mediaByLocationFetchingSelector = createDeepEqualSelector(
  [mediaByLocationArraySelector],
  (mediaByLocationArray) => {

    // detect if ANY location is fetching
    const areSomeLocationsFetching = mediaByLocationArray.some((media) => media.isFetching);
    return areSomeLocationsFetching;
  },
);

const mediaByLocationFetchedSelector = createDeepEqualSelector(
  [mediaByLocationArraySelector],
  (mediaByLocationArray) => {

    // detect if EVERY location has been fetched (just the first page)
    const areAllLocationsFetched = mediaByLocationArray.every((media) => media.isFetched);
    return areAllLocationsFetched;
  },
);

const getFirstNArrayItems = (allItems: any[], N: number) => allItems.slice(0, Math.min(N, allItems.length));

const filteredMediaItemsSelector = createDeepEqualSelector(
  [mediaByLocationSelector, selectedTagIdSelector],
  (mediaByLocation, selectedTagId) => {

    let mediaItems: MediaItem[] = [];
    // return first 20 items if a tag is selected
    if (selectedTagId) {
      const locationMedia = mediaByLocation[selectedTagId];
      if (locationMedia && locationMedia.items && locationMedia.items.length) {
        mediaItems = getFirstNArrayItems(locationMedia.items, MEDIA_PER_PAGE);
      }
    }
    // otherwise return first 20 items for each location
    else {

      Object.keys(mediaByLocation).forEach((locationId) => {
        const locationMedia = mediaByLocation[locationId];
        if (locationMedia && locationMedia.items && locationMedia.items.length) {
          mediaItems = mediaItems.concat(getFirstNArrayItems(locationMedia.items, MEDIA_PER_PAGE));
        }
      });
    }

    return mediaItems;
  },
);


export {
  mediaByLocationSelector,
  makeMediaByLocationIdItems,
  makeMediaByLocationIdIsFetching,
  makeMediaByLocationIdIsFetched,
  makeMediaByLocationIdFullyLoaded,
  makeMediaByLocationIdLazyLoadParams,
  mediaByLocationArraySelector,
  mediaByLocationFetchingSelector,
  mediaByLocationFetchedSelector,
  filteredMediaItemsSelector,
};
