/* ----------------------------------------------- */
/*   Reusable selectors to use in mapStateToProps
/*   so that the props only change when the actual bit of state they depend on changes
/*   https://redux.js.org/recipes/computing-derived-data
/*   https://redux-starter-kit.js.org/api/createselector
/*   https://github.com/reduxjs/reselect
/* ----------------------------------------------- */

import { createDeepEqualSelector } from 'AppSrc/core/selectors/create-deep-equal-selector';
import { AppState } from 'AppSrc/core/reducers/reducer-root';


const selectedTagIdSelector = (state: AppState) => (state.tags.selectedTag);
const colorMapSelector = (state: AppState) => (state.tags.colorMap);
const locationsByIdSelector = (state: AppState) => (state.tags.locationsById);
const allLocationIdsSelector = (state: AppState) => (state.tags.allLocations);


const selectedTagSelector = createDeepEqualSelector(
  [selectedTagIdSelector, locationsByIdSelector],
  (selectedTagId, locationsById) => (selectedTagId ? locationsById[selectedTagId] : null),
);

const allLocationsSelector = createDeepEqualSelector(
  [allLocationIdsSelector, locationsByIdSelector],
  (allLocationIds, locationsById) => (allLocationIds.map((tagId) => locationsById[tagId])),
);

// MM: 'makeSelector' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
// const getTagId = (state, props) => (state.tags.locationsById[props.tagId]);

// MM: it seems you don't have the pass the whole props as arguments
// here we let connect pass just tagId, as it sometimes comes on its own, sometimes from router match
// https://blog.isquaredsoftware.com/2017/12/idiomatic-redux-using-reselect-selectors/
const tagIdPropSelector = (state: AppState, tagId: string) => (tagId);

const makeGetTagById = () => (createDeepEqualSelector(
  [locationsByIdSelector, tagIdPropSelector],
  (locationsById, tagId) => (locationsById[tagId]),
));

const makeIsTagSelected = () => (createDeepEqualSelector(
  [selectedTagIdSelector, tagIdPropSelector],
  (selectedTagId, tagId) => (tagId === selectedTagId),
));


export {
  selectedTagIdSelector,
  colorMapSelector,
  locationsByIdSelector,
  allLocationIdsSelector,
  selectedTagSelector,
  allLocationsSelector,
  tagIdPropSelector,
  makeGetTagById,
  makeIsTagSelected,
};
