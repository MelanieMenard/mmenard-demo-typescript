/* ----------------------------------------------- */
/*   Reusable selectors to use in mapStateToProps
/*   so that the props only change when the actual bit of state they depend on changes
/*   https://redux.js.org/recipes/computing-derived-data
/*   https://redux-starter-kit.js.org/api/createselector
/*   https://github.com/reduxjs/reselect
/* ----------------------------------------------- */


import { createDeepEqualSelector } from 'AppSrc/core/selectors/create-deep-equal-selector';
import { locationsByIdSelector } from 'AppSrc/core/selectors/selectors-tags';
import { mediaByLocationSelector } from 'AppSrc/core/selectors/selectors-media-list';
import { AppState } from 'AppSrc/core/reducers/reducer-root';


const isMediaDetailFetchingSelector = (state: AppState) => (state.mediaDetail.isFetching);
const mediaByIdSelector = (state: AppState) => (state.mediaDetail.mediaById);

// MM: it seems you don't have the pass the whole props as arguments
// here we let connect pass just mediaId, as it sometimes comes on its own, sometimes from router match
// https://blog.isquaredsoftware.com/2017/12/idiomatic-redux-using-reselect-selectors/
const mediaIdPropSelector = (state: AppState, mediaId: string) => (mediaId);


// MM: 'makeSelector' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components

const makeMediaDetailById = () => (createDeepEqualSelector(
  [mediaByIdSelector, mediaIdPropSelector],
  (mediaById, mediaId) => (mediaById[mediaId] || null),
));

const makeMediaParentLocation = () => (createDeepEqualSelector(
  [locationsByIdSelector, mediaByLocationSelector, mediaIdPropSelector],
  (locationsById, mediaByLocation, mediaId) => {

    // MM: only those location Ids that have matching media, that's why we don't use allLocationIdsSelector
    const locationIds = Object.keys(mediaByLocation);

    const parentLocationId = locationIds.find((locationId) => mediaByLocation[locationId].items.some((media) => (media.id === mediaId)));
    const parentLocation = parentLocationId ? locationsById[parentLocationId] : null;

    return parentLocation;
  },
));


export {
  isMediaDetailFetchingSelector,
  mediaByIdSelector,
  makeMediaDetailById,
  makeMediaParentLocation,
};
