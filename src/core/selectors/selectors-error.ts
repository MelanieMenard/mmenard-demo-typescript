/* ----------------------------------------------- */
/*   Reusable selectors to use in mapStateToProps
/*   so that the props only change when the actual bit of state they depend on changes
/*   https://redux.js.org/recipes/computing-derived-data
/*   https://redux-starter-kit.js.org/api/createselector
/*   https://github.com/reduxjs/reselect
/* ----------------------------------------------- */

import { createSelector } from 'redux-starter-kit';
import { AppState } from 'AppSrc/core/reducers/reducer-root';

const errorsSelector = (state: AppState) => (state.error.errors);

const errorToDisplaySelector = createSelector(
  errorsSelector,
  (errors) => (errors[0] || null),
);

export {
  errorsSelector,
  errorToDisplaySelector,
};
