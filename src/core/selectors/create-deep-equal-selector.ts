import { createSelectorCreator, defaultMemoize } from 'reselect';
import isEqual from 'lodash.isequal';

// create a "selector creator" that uses lodash.isEqual instead of ===
// to avoid 'false positive upadates' when an unrelated element of an array is updated
const createDeepEqualSelector = createSelectorCreator(
  defaultMemoize,
  isEqual,
);

export { createDeepEqualSelector };
