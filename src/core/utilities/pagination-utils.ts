/* - paginatedArray: utility used by reducer to concatenate pages of items fetched from server into 1 array - */
// existingItems: array of items already in redux
// fetchedItems:new page of items fetched form the server
// totalItemsCount: number of total items to fetch from server
// offset: index of item we want to concat from
// perPage: items per page

interface PaginationDataInterface {
  paginationCase: string;
  paginatedItems: any[];
}

const paginatedArray = (existingItems: any[], fetchedItems: any[], totalItemsCount: number, offset: number, perPage: number): PaginationDataInterface => {

  const existingItemsCount = existingItems.length;
  const fetchedItemsCount = fetchedItems.length;

  const endIndex = offset + fetchedItemsCount - 1;

  // find out what type of page has been fetched?
  // is it items never fetched before ?
  const isNextItems = (offset === existingItemsCount);
  // is it refresh of existing items in the middle of the array?
  const isRefresh = ((offset < existingItemsCount) && (endIndex < existingItemsCount));
  // special case of refreshing the last page with also items appended or removed at the end
  const endItemsAdded = (endIndex >= existingItemsCount);
  const endItemsRemoved = (totalItemsCount < existingItemsCount);
  const isUpdateLastPage = ((offset < existingItemsCount) && (endItemsAdded || endItemsRemoved));

  const paginationData: PaginationDataInterface = {
    paginationCase: '',
    paginatedItems: [],
  };

  // append next items to end of existing array
  if (isNextItems) {

    paginationData.paginationCase = 'isNextItems';
    paginationData.paginatedItems = [
      ...existingItems,
      ...fetchedItems,
    ];

  }
  // replace refreshed items in array
  else if (isRefresh && !isUpdateLastPage) {

    paginationData.paginationCase = 'isRefresh';
    paginationData.paginatedItems = [
      ...existingItems.slice(0, offset),
      ...fetchedItems,
      ...existingItems.slice(endIndex + 1),
    ];

  }
  // special case of refreshing the last page with also items appended or removed at the end
  else if (isUpdateLastPage) {

    paginationData.paginationCase = 'isUpdateLastPage';
    paginationData.paginatedItems = [
      ...existingItems.slice(0, offset),
      ...fetchedItems,
    ];

  }
  // otherwise the pagination order got completely lost ??? What to do? replace the array since the new items are what we want to see?
  else {

    paginationData.paginationCase = 'paginationGotLost';
    paginationData.paginatedItems = [
      ...fetchedItems,
    ];
  }

  return paginationData;
};

export { paginatedArray };
