import React from 'react';
import { Provider } from 'react-redux';
import { BrowserRouter as Router } from 'react-router-dom';
import 'AppSrc/core/style/app.css';
import { store } from 'AppSrc/core/store/store';
import { AppLayout } from 'AppSrc/core/components/app-layout/app-layout';

const App = () => (
  <Provider store={store}>
    <Router>
      <AppLayout />
    </Router>
  </Provider>
);


export { App };
