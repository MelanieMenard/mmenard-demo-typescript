import React from 'react';
import { Link } from 'react-router-dom';
import './breadcrumb.css';
import { BreadcrumbInterface } from 'AppSrc/core/interfaces/Breadcrumb';

const Breadcrumb = React.memo<{ breadcrumbs: BreadcrumbInterface[] }>(({ breadcrumbs }) => (
  <ul className="breadcrumbs">
    {breadcrumbs.map((breadcrumb, index) => (
      <li className="breadcrumb" key={breadcrumb.link}>
        { (index > 0) && (
          <span className="breadcrumb-separator">&gt;</span>
        )}
        <Link className="breadcrumb-link" to={breadcrumb.link}>{breadcrumb.title}</Link>
      </li>
    ))}
  </ul>
));

export { Breadcrumb };
