import React from 'react';
import { mount } from 'enzyme';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { Breadcrumb } from './breadcrumb';

// fixture
const breadcrumbs = [
  {
    link: '/page1',
    title: 'Page 1',
  },
  {
    link: '/page2',
    title: 'Page 2',
  },
]

const BreadcrumbWithRouter = (
  <TestRouter>
    <Breadcrumb
      breadcrumbs={breadcrumbs}
    />
  </TestRouter>
);

const BreadcrumbTest = (
  <Breadcrumb
    breadcrumbs={breadcrumbs}
  />
);

describe('Breadcrumb Component', () => {
  rendersWithoutCrashing(BreadcrumbWithRouter);
  matchesThePreviousSnapshot(BreadcrumbTest, {shallow: true});
});