import React, { Fragment, useEffect, useMemo } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchMediaDetail } from 'AppSrc/core/actions/actions-media-detail';
import { Breadcrumb } from 'AppSrc/core/components/breadcrumb/breadcrumb';
import { Loader } from 'AppSrc/core/components/loader/loader';
import { findMediaParentLocation } from 'AppSrc/core/data-structures/location-utils';
import './media-detail.css';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbInterface } from 'AppSrc/core/interfaces/Breadcrumb';
import { Location } from 'AppSrc/core/interfaces/Location';
import { MediaDetailInterface } from 'AppSrc/core/interfaces/Media';
import {
  isMediaDetailFetchingSelector,
  makeMediaDetailById,
  makeMediaParentLocation,
} from 'AppSrc/core/selectors/selectors-media-detail';


interface  MediaDetailDisplayProps {
  match: RouteComponentProps<{ mediaId: string }>;
  mediaId: string;
  media: MediaDetailInterface | null;
  parentLocation: Location | null;
  isFetching: boolean;
  fetchMediaDetailData(mediaId: string): void;
}


// utility function to make breadcrumbs from parent location
const makeBreadcrumbsFromParentLocation = (parentLocation: Location | null): BreadcrumbInterface[] => {
  const breadcrumbs: BreadcrumbInterface[] = [
    {
      link: '/',
      title: 'Location map',
    },
  ];

  // parentLocation provided via mapStateToProps
  // if we arrived at the media detail screen from a media list, parentLocation will be in redux as the media list will have been fetched
  // however parentLocation will be null if we directly loaded the app on the media detail route, as media lists won't have been fetched
  if (parentLocation) {
    breadcrumbs.push({
      link: `/location/${parentLocation.id}`,
      title: parentLocation.displayName,
    });
  }
  return breadcrumbs;
};


/* --- Media detail presentational component --- */
const MediaDetailDisplay = React.memo<MediaDetailDisplayProps>(({
  match,
  mediaId,
  media,
  parentLocation,
  isFetching,
  fetchMediaDetailData,
}) => {

  // fetch the media if not already doing so or already cached
  useEffect(
    () => {
      if (!isFetching && !media) {
        fetchMediaDetailData(mediaId);
      }
    },
    [isFetching, media, mediaId, fetchMediaDetailData],
  );

  // useMemo hook means breadcrumbs are only recalculated on render if parentLocation has changed
  const breadcrumbs = useMemo(() => makeBreadcrumbsFromParentLocation(parentLocation), [parentLocation]);

  const userMessage = (!isFetching && (media === null)) ? 'No Image Data.' : '';
  const imageMeta = media ? `Taken by ${media.author} on ${media.dateTaken}` : '';

  const mediaTitle = (media && media.title) ? media.title : '';
  const mediaLink = (media && media.link) ? media.link : '';
  const mediaDescription = (media && media.description) ? media.description : '';


  return (
    <div className="media-detail">
      {isFetching && (
        <Loader />
      )}
      {userMessage && (
        <div className="message">
          <p>{userMessage}</p>
        </div>
      )}
      {(!isFetching && !userMessage) && (
        <Fragment>
          <Breadcrumb
            breadcrumbs={breadcrumbs}
          />
          <h2 className="media-detail-title">{mediaTitle}</h2>
          <p className="media-detail-meta">{imageMeta}</p>
          <img className="media-detail-image" src={mediaLink} alt={mediaTitle} />
          <p className="media-detail-description">{mediaDescription}</p>
        </Fragment>
      )}
    </div>
  );

});


/* --- Media detail container component --- */


// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getMediaDetailById = makeMediaDetailById();
  const getMediaParentLocation = makeMediaParentLocation();

  const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{ mediaId: string }>) => ({
    mediaId: ownProps.match.params.mediaId,
    media: getMediaDetailById(state, ownProps.match.params.mediaId),
    parentLocation: getMediaParentLocation(state, ownProps.match.params.mediaId),
    isFetching: isMediaDetailFetchingSelector(state),
  });
  return mapStateToProps;
};

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>, ownProps: RouteComponentProps<{ mediaId: string }>) => ({
  fetchMediaDetailData: (mediaId: string): void => {
    dispatch(fetchMediaDetail(mediaId));
  },
});

const MediaDetail = withRouter(connect(
  makeMapStateToProps,
  mapDispatchToProps,
)(MediaDetailDisplay));


export {
  MediaDetailDisplay,
  MediaDetail,
};
