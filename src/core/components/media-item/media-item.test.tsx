import React from 'react';
import { mount } from 'enzyme';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { media1 } from 'AppSrc/core/test/fixtures/media-list-fixtures';
import { Media } from './media-item';

const MediaItemWithRouter = (
  <TestRouter>
    <Media
      media={media1}
    />
  </TestRouter>
);

const MediaItem = (
  <Media
    media={media1}
  />
);

describe('Media Item Component', () => {
  rendersWithoutCrashing(MediaItemWithRouter);
  matchesThePreviousSnapshot(MediaItem, {shallow: true});
});
