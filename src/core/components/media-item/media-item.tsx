import React from 'react';
import { Link } from 'react-router-dom';
import './media-item.css';
import { MediaItem } from 'AppSrc/core/interfaces/Media';


/* --- Media item presentational component --- */
const Media = React.memo<{ media: MediaItem }>(({ media }) => {

  const inlineStyle = {
    backgroundImage: `url(${media.image})`,
  };

  return (
    <li className="media-item">
      <Link to={`/media/${media.id}`} className="media-item-link">
        <div className="media-item-image" style={inlineStyle} />
        <div className="media-item-info">
          <p className="media-item-title">{media.title}</p>
        </div>
      </Link>
    </li>
  );
});


export { Media };
