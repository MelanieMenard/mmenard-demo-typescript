import React, { PureComponent, ReactNode } from 'react';


/* --- Lazy loader component --- */
// lazyLoad: function that will getch a page of data when lazyloader detects it is necessary. takes as input lazyLoadParams below
// lazyLoadParams: parameters for the paginated request to fetch a page of data
// fullyLoaded: true if all items have been fetched from server
// bottomOffset = allow positive or negative space at the bottom of the screen

interface Pagination {
  page: number;
  perPage: number;
}

export interface LazyLoadParams {
  paginationData: Pagination;
  isFetching: boolean;
  params: any;
}

export interface LazyLoaderProps {
  lazyLoadParams: LazyLoadParams;
  fullyLoaded: boolean;
  children: ReactNode;
  bottomOffset: number;
  lazyLoad(lazyLoadParams: LazyLoadParams): void;
}


class LazyLoader extends PureComponent<LazyLoaderProps> {

  private previousLazyLoadParams: LazyLoadParams | null;

  constructor(props: LazyLoaderProps) {
    super(props);
    this.previousLazyLoadParams = null;
    this.shouldILoad = this.shouldILoad.bind(this);
    this.lazyLoad = this.lazyLoad.bind(this);
  }

  public componentDidMount() {
    this.setScrollEvent();
    // gets first page data and then checks if needs more to fill the screen
    this.shouldILoad();
  }

  public componentDidUpdate() {
    this.shouldILoad();
  }

  public componentWillUnmount() {
    this.unsetScrollEvent();
  }

  // listen to scroll events
  public setScrollEvent() {
    document.addEventListener('scroll', this.shouldILoad);
  }

  public unsetScrollEvent() {
    document.removeEventListener('scroll', this.shouldILoad);
  }

  // fetch a page of data
  public lazyLoad() {
    const {
      lazyLoadParams,
    } = this.props;

    if (lazyLoadParams.isFetching) {
      // no ops if it's already fetching
      return null;
    }

    const previousLazyLoadParamsJSON = JSON.stringify(this.previousLazyLoadParams);
    const lazyLoadParamsJSON = JSON.stringify(lazyLoadParams);

    // Blocks multiple identical requests
    if (previousLazyLoadParamsJSON === lazyLoadParamsJSON) {
      // no ops if the request has the same paramaters as the last
      return null;
    }

    this.props.lazyLoad(lazyLoadParams);
    this.previousLazyLoadParams = lazyLoadParams;
    return null;
  }

  // check whether we need to fetch more items to fill the visible screen
  public shouldILoad() {

    const {
      bottomOffset,
      fullyLoaded,
    } = this.props;

    // stops trying to make requests when all the data has been fetched
    if (fullyLoaded) {
      this.unsetScrollEvent();
      return null;
    }

    // height scrollable document is
    const scrollHeight = document.documentElement.scrollHeight;
    // height currently visisble in the browser
    const offsetHeight = document.documentElement.offsetHeight;
    // top of the viewable screen
    const scrollTop = document.documentElement.scrollTop;
    // bottom of the viewable screen (the browser doesn't give us this so we calculate it ourself)
    const scrollBottom = scrollTop + offsetHeight;

    const scrolledToBottomOfDocument = (scrollBottom + bottomOffset) >= scrollHeight;

    if (scrolledToBottomOfDocument) {
      this.lazyLoad();
    }
    return null;
  }

  public render() {
    const { children } = this.props;
    return (
      <React.Fragment>
        {children}
      </React.Fragment>
    );
  }
}


export { LazyLoader };
