import React, { PureComponent, SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import { setSelectedTag } from 'AppSrc/core/actions/actions-tags';
import './tag-cloud.css';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { Location } from 'AppSrc/core/interfaces/Location';
import {
  makeGetTagById,
  makeIsTagSelected,
} from 'AppSrc/core/selectors/selectors-tags';

interface  TagOwnProps {
  tagId: string;
}

interface TagDisplayProps extends TagOwnProps {
  tag: Location;
  tagId: string;
  isSelected: boolean;
  selectTag(): void;
}

/* --- Tag item presentational component --- */
const TagDisplay = React.memo<TagDisplayProps>(({
  tag,
  tagId,
  isSelected,
  selectTag,
}) => {

  const matchingItemsString = (tag.matchingItems) ? ` (${tag.matchingItems})` : '';

  return (
    <li className={`tag-item popularity-${tag.popularity}${isSelected ? ' selected' : ''}`}>
      <button
        type="button"
        className="tag"
        onClick={selectTag}
      >
        <p className="tag-title">
          {tag.displayName}
          {matchingItemsString}
        </p>
      </button>
    </li>
  );
});


/* --- Tag item container component --- */


// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getTagById = makeGetTagById();
  const isTagSelected = makeIsTagSelected();

  const mapStateToProps = (state: AppState, ownProps: TagOwnProps) => ({
    tag: getTagById(state, ownProps.tagId),
    isSelected: isTagSelected(state, ownProps.tagId),
  });
  return mapStateToProps;
};

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>, ownProps: TagOwnProps) => ({
  // Set selected tag
  selectTag: (): void => {
    dispatch(setSelectedTag(ownProps.tagId));
  },
});

const Tag = connect(
  makeMapStateToProps,
  mapDispatchToProps,
)(TagDisplay);


// 'Display' unconnected presentational components are exported for testing only
export {
  TagDisplay,
  Tag,
};
