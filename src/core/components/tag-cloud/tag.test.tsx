import React from 'react';
import { mount } from 'enzyme';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { tag1 } from 'AppSrc/core/test/fixtures/tag-fixtures';
import { TagDisplay } from './tag';


/* Test a single tag presentational component on its own */

// mock function for setSelectedTag
const selectTag = jest.fn();
const UnconnectedTagNotSelected = <TagDisplay
    tagId={tag1.id}
    tag={tag1}
    tagPopularity={3}
    isSelected={false}
    selectTag={selectTag}
  />;
const UnconnectedTagSelected = <TagDisplay
    tagId={tag1.id}
    tag={tag1}
    tagPopularity={3}
    isSelected={true}
    selectTag={selectTag}
  />;

const clickingButtonSetSelectedTag = () => {
  it('calls selectTag when the button is clicked', () => {
    const wrapper = mount(UnconnectedTagNotSelected);
    wrapper.find('button').simulate('click', { preventDefault() {} });
    expect(selectTag).toHaveBeenCalled();
  });
};

describe('Unconnected Tag Component', () => {
  rendersWithoutCrashing(UnconnectedTagNotSelected);
  matchesThePreviousSnapshot(UnconnectedTagSelected, {shallow: true, snapshotName: 'tag-selected'});
  matchesThePreviousSnapshot(UnconnectedTagNotSelected, {shallow: true, snapshotName: 'tag-not-selected'});
  clickingButtonSetSelectedTag();
});