import React from 'react';
import { mount } from 'enzyme';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { tagIdsArray } from 'AppSrc/core/test/fixtures/tag-fixtures';
import { TagCloudDisplay } from './tag-cloud';


/* Test the presentational tag cloud component while mocking tag items */

// mock out the Tag component to keep the snapshot small
// we can't use 'shallow' because the Tag needs to have a Provider in its surrounding context
// When a component needs to be wrapped in a Provider or Router, we can't use shallow, we need to mock them instead
jest.mock('./tag', () => (
  { Tag: 'mock-tag' }
));

const UnconnectedTagCloud = <TagCloudDisplay
  tags={tagIdsArray}
/>

describe('Unconnected Tag Cloud Component', () => {
  rendersWithoutCrashing(UnconnectedTagCloud);
  matchesThePreviousSnapshot(UnconnectedTagCloud, {shallow: true});
});