import React from 'react';
import { Link } from 'react-router-dom';
import './location-detail-link.css';
import { Location } from 'AppSrc/core/interfaces/Location';

const LocationDetailLink = React.memo<{ location: Location }>(({ location }) => (
  <p className="location-detail-more">
    <Link to={`/location/${location.id}`} className="location-detail-link">
      {`More on ${location.displayName} `}
      &rarr;
    </Link>
  </p>
));

export { LocationDetailLink };
