import React from 'react';
import { mount } from 'enzyme';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { LocationDetailLink } from './location-detail-link';
import { tag1 } from 'AppSrc/core/test/fixtures/tag-fixtures';

const LocationDetailLinkWithRouter = (
  <TestRouter>
    <LocationDetailLink
      location={tag1}
    />
  </TestRouter>
);

const LocationDetailLinkTest = (
  <LocationDetailLink
    location={tag1}
  />
);

describe('LocationDetailLink Component', () => {
  rendersWithoutCrashing(LocationDetailLinkWithRouter);
  matchesThePreviousSnapshot(LocationDetailLinkTest, {shallow: true});
});