import React, { PureComponent, RefObject } from 'react';
import { connect } from 'react-redux';
import * as d3 from 'd3';
import './location-map.css';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { Location, ColorMap } from 'AppSrc/core/interfaces/Location';
import {
  colorMapSelector,
  selectedTagIdSelector,
  allLocationsSelector,
} from 'AppSrc/core/selectors/selectors-tags';


interface LocationMapDisplayProps {
  locations: Location[];
  selectedLocation: string | null;
  colorMap: ColorMap;
}

interface LocationMapDisplayState {
  chartWidth: number;
  chartHeight: number;
}


class LocationMapDisplay extends PureComponent<LocationMapDisplayProps, LocationMapDisplayState> {

  private containerEl: RefObject<HTMLDivElement>;
  private svgEl: RefObject<SVGSVGElement>;

  constructor(props: LocationMapDisplayProps) {
    super(props);

    this.state = {
      chartWidth: 0,
      chartHeight: 0,
    };
    this.containerEl = React.createRef();
    this.svgEl = React.createRef();
    this.initChartDimensions = this.initChartDimensions.bind(this);
  }

  public componentDidMount() {
    this.initChartDimensions();
    window.addEventListener('resize', this.initChartDimensions);
  }

  public componentDidUpdate() {
    this.updateChart();
  }

  public componentWillUnmount() {
    window.removeEventListener('resize', this.initChartDimensions);
  }

  // dynamically init canvas width based on screen size
  // https://stackoverflow.com/questions/36862334/get-viewport-window-height-in-reactjs
  // https://stackoverflow.com/questions/49058890/how-to-get-a-react-components-size-height-width-before-render
  public initChartDimensions() {

    // MM: keep TS happy that it can't be null;
    const clientWidth = (this.containerEl && this.containerEl.current) ? this.containerEl.current.clientWidth : 1024;

    const chartWidth = Math.min(clientWidth, 1024);
    const chartHeight = Math.round(chartWidth * 2 / 3);
    console.log('initChartDimensions: ', chartWidth, chartHeight);

    this.setState({
      chartWidth,
      chartHeight,
    });
  }

  // MM TODO REFACTOR TS HACK as any
  // get the D3 TS types to work
  // https://stackoverflow.com/questions/40756344/d3-v4-typescript-types
  public updateChart() {

    const { chartWidth, chartHeight } = this.state;
    const { colorMap, selectedLocation, locations } = this.props;

    const latitudes = locations.map((locationData) => locationData.latitude).sort();
    const longitudes = locations.map((locationData) => locationData.longitude).sort();

    const minLat = latitudes.length ? latitudes[0] : 0;
    const maxLat = latitudes.length ? latitudes[latitudes.length - 1] : 0;
    const minLon = longitudes.length ? longitudes[0] : 0;
    const maxLon = longitudes.length ? longitudes[latitudes.length - 1] : 0;
    const rangeLat = maxLat - minLat;
    const rangeLon = maxLon - minLon;
    const centerLat = (maxLat + minLat) / 2;
    const centerLon = (maxLon + minLon) / 2;

    // calculate scale for the map
    // best scale that would fit the longitude range on the width of the image
    // whole world has a longitude range of 360, from -180 to +180
    const scaleLon = Math.round((360 * chartWidth) / (maxLon - minLon));
    // best scale that would fit the latitude range on the height of the image
    // whole world has a latitude range of 180, from -90 to +90
    const scaleLat = Math.round((180 * chartHeight) / (maxLat - minLat));

    // MM: mapZoom is a mystery coefficient that seems to make most range of lat/log fit nicely on screen
    // 8 works best for 'worldwide' type data where the range is > 100
    // otherwise (for Europe, country or city type data) 7 works best
    // I have no idea where these numbers come from, or the math behind them, found by trial and error
    const mapRange = Math.round(Math.max(rangeLat, rangeLon));
    const mapZoom = (mapRange > 100) ? 8 : 7;
    const mapScale = Math.round(Math.min(scaleLon, scaleLat) / mapZoom);

    const center = [centerLon, centerLat];

    // MM TODO REFACTOR TS HACK as any
    // TS gets confused by arrays of numbers
    // https://github.com/Microsoft/TypeScript/issues/12579
    const projection = d3.geoMercator()
      .center(center as any)
      .scale(mapScale)
      .translate([chartWidth / 2, chartHeight / 2]);

    // Follows d3 general update pattern:
    // https://www.d3indepth.com/enterexit/#general-update-pattern
    // And tuturial to integrate D3 in react components
    // https://frontendcharts.com/react-d3-integrate/
    // https://codepen.io/frontendcharts/pen/NBYLvy
    // http://jsfiddle.net/3bzsE/
    // https://bl.ocks.org/d3indepth/e890d5ad36af3d949f275e35b41a99d6

    // get the svg ref from react
    const svg = d3.select(this.svgEl.current);

    // Define the data for the locations, and use id as unique key
    const location = svg.selectAll('.location')
      .data(locations, (d) => (d as any).id);

    // Create and place the "blocks" containing the circle and the text
    const locationEnter = location.enter()
      .append('g')
      .classed('location', true)
      .classed('selected', (d) => ((d.id === selectedLocation)))
      .attr('id', (d) => `location-${d.id}`);

    // create the location circles with initial style inside each block
    locationEnter.append('circle')
      .classed('location-circle', true)
      .attr('r', 1)
      .attr('cx', chartWidth / 2)
      .attr('cy', chartHeight / 2)
      .attr('fill', '#fff')
      .attr('fill-opacity', 0);

    // create the location labels with initial style inside each block
    locationEnter.append('text')
      .classed('location-label', true)
      .style('text-anchor', 'start')
      .text((d) => d.displayName)
      .attr('font-size', '11px')
      .attr('fill', (d) => (colorMap[d.type] || colorMap.other))
      .attr('dx', chartWidth / 2)
      .attr('dy', chartHeight / 2);

    // add class selected when the data updates
    locationEnter
      .merge(location as any)
      .classed('selected', (d) => ((d.id === selectedLocation)));

    // move selected element to the top (D3 svg version of z-index)
    svg.select('.location.selected')
      .raise();

    // animate the location circles when the data updates
    locationEnter
      .merge(location as any)
      .select('.location-circle')
      .transition()
      .duration(1000)
      .attr('cx', (d) => (projection as any)([d.longitude, d.latitude])[0])
      .attr('cy', (d) => (projection as any)([d.longitude, d.latitude])[1])
      .attr('r', (d) => d.popularityWideScale)
      .attr('fill', (d) => (colorMap[d.type] || colorMap.other))
      .attr('fill-opacity', (d) => ((d.id === selectedLocation) ? 1 : 0.7));

    // animate the location labels when the data updates
    locationEnter
      .merge(location as any)
      .select('.location-label')
      .transition()
      .duration(1000)
      .attr('font-size', (d) => ((d.id === selectedLocation) ? '18px' : '11px'))
      .attr('dx', (d) => ((projection as any)([d.longitude, d.latitude])[0] + ((d.id === selectedLocation) ? 18 : 10)))
      .attr('dy', (d) => ((projection as any)([d.longitude, d.latitude])[1] - 6));

    // remove the outdated elements
    location.exit().remove();

  }

  public renderChart() {
    const { chartWidth, chartHeight } = this.state;
    return (
      <svg className="location-map-svg" width={chartWidth} height={chartHeight} ref={this.svgEl} />
    );
  }

  public render() {
    const { chartWidth, chartHeight } = this.state;
    return (
      <div className="location-map" ref={this.containerEl}>
        {chartWidth && chartHeight && this.renderChart()}
      </div>
    );
  }
}


/* --- Location Map container component --- */
// https://redux.js.org/basics/usage-with-react#implementing-container-components
// the container component is generated by react-redux
// it feeds the data from the redux store to the presentational component


const mapStateToProps = (state: AppState, ownProps: {}) => ({
  locations: allLocationsSelector(state),
  selectedLocation: selectedTagIdSelector(state),
  colorMap: colorMapSelector(state),
});


const LocationMap = connect(
  mapStateToProps,
)(LocationMapDisplay);


// 'Display' unconnected presentational components are exported for testing only
export {
  LocationMapDisplay,
  LocationMap,
};
