import React from 'react';
import {
  TestRouter,
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { AppLayoutDisplay } from './app-layout';

// mock out child components to keep the snapshot small
jest.mock('AppSrc/core/components/home-screen/home-screen', () => (
  { HomeScreen: 'mock-home-screen' }
));
jest.mock('AppSrc/core/components/media-detail/media-detail', () => (
  { MediaDetail: 'mock-media-detail' }
));
jest.mock('AppSrc/core/components/location-detail/location-detail', () => (
  { LocationDetail: 'mock-location-detail' }
));

const AppLayoutWithRouter = (
  <TestRouter>
    <AppLayoutDisplay
      error={null}
      clearDisplayedError={()=> null}
    />
  </TestRouter>
);

const AppLayoutTest = (
  <AppLayoutDisplay
    error={null}
    clearDisplayedError={()=> null}
  />
);

describe('Unconnected AppLayout', () => {
  rendersWithoutCrashing(AppLayoutWithRouter);
  // Match against a shallow snapshot. Router not needed
  matchesThePreviousSnapshot(AppLayoutTest, {shallow: true});
});
