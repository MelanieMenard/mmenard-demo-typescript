import React, { useState, SyntheticEvent } from 'react';
import { connect } from 'react-redux';
import { Route, Switch, Link } from 'react-router-dom';
import { Snackbar, Button } from '@material-ui/core';
import './app-layout.css';
import { removeDisplayedError } from 'AppSrc/core/actions/actions-error';
import { errorToDisplaySelector } from 'AppSrc/core/selectors/selectors-error';
import { HomeScreen } from 'AppSrc/core/components/home-screen/home-screen';
import { MediaDetail } from 'AppSrc/core/components/media-detail/media-detail';
import { LocationDetail } from 'AppSrc/core/components/location-detail/location-detail';
// MM: if I had my own own custom API, this would come from an endpoint, most likely graphQL, as it contains all the base app config at once
import AppConfig from 'AppSrc/core/data/app-config.json';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { ThunkDispatch } from 'redux-thunk';
import { FormattedError } from 'AppSrc/core/interfaces/Error';


interface AppLayoutDisplayProps {
  error: FormattedError | void;
  clearDisplayedError(): void;
}

interface AppLayoutDisplayState {
  showError: boolean;
}

const AppLayoutDisplay = React.memo<AppLayoutDisplayProps>(({
  error,
  clearDisplayedError,
}) => {

  const [showError, setShowError] = useState(true);

  // animate the error snackbar away
  const closeSnackbar = (e: SyntheticEvent) => {
    setShowError(false);
  };

  // after the snackbar is animated away, clear the displayed message from redux
  // put back the display, in case there is another error to show
  const afterSnackbarClosed = () => {
    clearDisplayedError();
    setShowError(true);
  };

  const errorMessage = error ? `Error ${error.code}: ${error.message}` : '';
  const hasError = (error !== null);

  return (
    <div className="app">

      <header className="app-header">
        <Link className="app-homelink" to="/">
          <h1 className="app-title">{AppConfig.appInfo.title}</h1>
          {AppConfig.appInfo.tagline && (
            <h2 className="app-tagline">{AppConfig.appInfo.tagline}</h2>
          )}
        </Link>
      </header>

      <div className="app-content">
        <Switch>
          <Route exact path="/" component={HomeScreen} />
          <Route path="/media/:mediaId" component={MediaDetail} />
          <Route path="/location/:locationId" component={LocationDetail} />
          <Route path="*" component={HomeScreen} />
        </Switch>
      </div>

      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        open={showError && hasError}
        message={errorMessage}
        onClose={closeSnackbar}
        onExited={afterSnackbarClosed}
        action={(
          <Button
            className="MuiButton-styled"
            variant="contained"
            aria-label="OK"
            onClick={closeSnackbar}
          >
            OK
          </Button>
        )}
      />

    </div>
  );

});


/* --- AppLayout Redux-connected container component --- */

const mapStateToProps = (state: AppState, ownProps: {}) => ({
  error: errorToDisplaySelector(state),
});

// mapDispatchToProps tells the container component how to dispatch actions to the redux store
const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, void, AnyAction>) => ({
  clearDisplayedError: (): void => {
    dispatch(removeDisplayedError());
  },
});

const AppLayout = connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppLayoutDisplay);


export {
  AppLayoutDisplay,
  AppLayout,
};
