import React from 'react';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { HomeScreenDisplay } from './home-screen';

// mock out child components to keep the snapshot small
jest.mock('AppSrc/core/components/tag-cloud/tag-cloud', () => (
  { TagCloud: 'mock-tag-cloud' }
));
jest.mock('AppSrc/core/components/filtered-media-list/filtered-media-list', () => (
  { FilteredMediaList: 'mock-media-list' }
));
jest.mock('AppSrc/core/components/location-map/location-map', () => (
  { LocationMap: 'mock-location-map' }
));

const HomeScreen = (
    <HomeScreenDisplay selectedLocation={null} />
);

describe('HomeScreen component', () => {
  rendersWithoutCrashing(HomeScreen);
  // Match against a shallow snapshot.
  matchesThePreviousSnapshot(HomeScreen, {shallow: true});
});