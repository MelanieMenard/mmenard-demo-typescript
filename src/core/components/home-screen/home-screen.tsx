import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { TagCloud } from 'AppSrc/core/components/tag-cloud/tag-cloud';
import { LocationMap } from 'AppSrc/core/components/location-map/location-map';
import { FilteredMediaList } from 'AppSrc/core/components/filtered-media-list/filtered-media-list';
import { LocationDetailLink } from 'AppSrc/core/components/location-detail-link/location-detail-link';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { RouteComponentProps } from 'react-router';
import { Location } from 'AppSrc/core/interfaces/Location';
import { selectedTagSelector } from 'AppSrc/core/selectors/selectors-tags';

interface HomeScreenDisplayProps extends RouteComponentProps {
  selectedLocation: Location | null;
}

const HomeScreenDisplay = React.memo<HomeScreenDisplayProps>(({ selectedLocation }) => (
  <Fragment>
    <div className="app-full-width-content">
      <LocationMap />
    </div>
    <div className="app-sidebar">
      <TagCloud />
    </div>
    <div className="app-main-content">
      {selectedLocation && (
        <LocationDetailLink
          location={selectedLocation}
        />
      )}
      <FilteredMediaList />
    </div>
  </Fragment>
));


/* --- HomeScreen container component --- */

const mapStateToProps = (state: AppState, ownProps: RouteComponentProps) => ({
  selectedLocation: selectedTagSelector(state),
});

const HomeScreen = connect(
  mapStateToProps,
)(HomeScreenDisplay);


export {
  HomeScreenDisplay,
  HomeScreen,
};
