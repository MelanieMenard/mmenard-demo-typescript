import React, { PureComponent } from 'react';
import { Loader } from 'AppSrc/core/components/loader/loader';
import { ScrollTop } from 'AppSrc/core/components/scroll-top/scroll-top';
import { LazyLoader, LazyLoadParams } from 'AppSrc/core/components/lazy-loader/lazy-loader';
import { Media } from 'AppSrc/core/components/media-item/media-item';
import { MEDIA_PER_PAGE } from 'AppSrc/core/queries/axios-queries';
import 'AppSrc/core/components/media-list/media-list.css';
import { MediaItem } from 'AppSrc/core/interfaces/Media';


/* --- Media list with lazyloader presentational component --- */


export interface MediaListLazyLoadProps {
  mediaItems: MediaItem[];
  lazyLoadParams: LazyLoadParams;
  fullyLoaded: boolean;
  isFetched: boolean;
  fetchMediaPage(lazyLoadParams: LazyLoadParams): void;
}

class MediaListLazyLoad extends PureComponent<MediaListLazyLoadProps> {

  constructor(props: MediaListLazyLoadProps) {
    super(props);
    this.lazyLoadPage = this.lazyLoadPage.bind(this);
  }

  // used by lazyloader when a page needs to be fetched
  public lazyLoadPage(lazyLoadParams: LazyLoadParams) {
    this.props.fetchMediaPage(lazyLoadParams);
  }

  public render() {

    const {
      mediaItems,
      lazyLoadParams,
      isFetched,
      fullyLoaded,
    } = this.props;

    const {
      isFetching,
    } = lazyLoadParams;

    const noMatchingItems = !isFetching && isFetched && !mediaItems.length;
    const userMessage = noMatchingItems ? 'No matching images found.' : '';

    const showScrollTop = fullyLoaded && !noMatchingItems && (mediaItems.length > MEDIA_PER_PAGE);

    return (
      <LazyLoader
        lazyLoad={this.lazyLoadPage}
        lazyLoadParams={lazyLoadParams}
        fullyLoaded={fullyLoaded}
        bottomOffset={150}
      >
        <div className="media-list-wrapper">
          {userMessage && (
            <div className="message">
              <p>{userMessage}</p>
            </div>
          )}
          {(!userMessage) && (
            <ul className="media-list">
              {mediaItems.map((media) => (
                <Media
                  key={media.id}
                  media={media}
                />
              ))}
            </ul>
          )}
          {isFetching && (
            <Loader />
          )}
          {showScrollTop && (
            <ScrollTop />
          )}
        </div>
      </LazyLoader>
    );
  }
}


export { MediaListLazyLoad };
