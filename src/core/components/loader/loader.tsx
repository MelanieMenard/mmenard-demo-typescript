import React from 'react';
import './loader.css';

const Loader = React.memo<{ loaderStyle?: string }>(({ loaderStyle }) => {
  const loaderStyleClass = loaderStyle || 'spinning';
  const className = `loader ${loaderStyleClass}`;
  return (
    <div className={className} />
  );
});

export { Loader };
