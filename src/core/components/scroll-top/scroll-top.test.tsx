import React from 'react';
import { mount } from 'enzyme';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { ScrollTop } from './scroll-top';

describe('ScrollTop Component', () => {
  rendersWithoutCrashing(<ScrollTop />);
  matchesThePreviousSnapshot(<ScrollTop />, {shallow: true});
});