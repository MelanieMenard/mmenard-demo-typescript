import React, { SyntheticEvent } from 'react';
import './scroll-top.css';

const ScrollTop = React.memo<{}>(({}) => {

  const scrollToTop = (e: SyntheticEvent) => {
    e.preventDefault();
    window.scrollTo(0, 0);
  };

  return (
    <div className="scroll-top-wrapper">
      <button
        type="button"
        className="scroll-top"
        onClick={scrollToTop}
      >
        Back to top
        &uarr;
      </button>
    </div>
  );
});


export { ScrollTop };
