import React, { useEffect } from 'react';
import { Loader } from 'AppSrc/core/components/loader/loader';
import { ScrollTop } from 'AppSrc/core/components/scroll-top/scroll-top';
import { Media } from 'AppSrc/core/components/media-item/media-item';
import { MEDIA_PER_PAGE } from 'AppSrc/core/queries/axios-queries';
import './media-list.css';
import { MediaItem } from 'AppSrc/core/interfaces/Media';


/* --- Media list presentational component --- */

interface  MediaListProps {
  mediaItems: MediaItem[];
  isFetching: boolean;
  isFetched: boolean;
  fetchMedia(): void;
}

const MediaList = React.memo<MediaListProps>(({
  mediaItems,
  isFetching,
  isFetched,
  fetchMedia,
}) => {

  // fetch the media if not already doing so or already cached
  useEffect(
    () => {
      if (!isFetching && !isFetched) {
        fetchMedia();
      }
    },
    [isFetching, isFetched, fetchMedia],
  );


  // show spinner if we are fetching and no items yet returned (otherwise show what is there and update as more get fetched)
  const showSpinner = isFetching && !mediaItems.length;
  const noMatchingItems = isFetched && !mediaItems.length;
  const userMessage = noMatchingItems ? 'No matching images found.' : '';
  const showScrollTop = (mediaItems.length > MEDIA_PER_PAGE);

  return (
    <div className="media-list-wrapper">

      {showSpinner && (
        <Loader />
      )}
      {userMessage && (
        <div className="message">
          <p>{userMessage}</p>
        </div>
      )}
      {(!showSpinner && !userMessage) && (
        <ul className="media-list">
          {mediaItems.map((media) => (
            <Media
              key={media.id}
              media={media}
            />
          ))}
        </ul>
      )}
      {showScrollTop && (
        <ScrollTop />
      )}

    </div>
  );
});

export { MediaList };
