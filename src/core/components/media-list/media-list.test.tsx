import React from 'react';
import { mount } from 'enzyme';
import {
  rendersWithoutCrashing,
  matchesThePreviousSnapshot,
} from 'AppSrc/core/test/utils/component-test-utils';
import { mediaListPage1 } from 'AppSrc/core/test/fixtures/media-list-fixtures';
import { MediaList } from './media-list';

jest.mock('AppSrc/core/components/media-item/media-item', () => (
  { Media: 'mock-media-item' }
));

jest.mock('AppSrc/core/components/loader/loader', () => (
  { Loader: 'mock-loader' }
));

const MediaListWithItems = (
  <MediaList
    mediaItems={mediaListPage1}
    isFetching={false}
    isFetched={true}
    fetchMedia={()=> null}
  />
);

const MediaListFetching = (
  <MediaList
    mediaItems={[]}
    isFetching={true}
    isFetched={false}
    fetchMedia={()=> null}
  />
);

const MediaListNoMatchingItems = (
  <MediaList
    mediaItems={[]}
    isFetching={false}
    isFetched={true}
    fetchMedia={()=> null}
  />
);


describe('Media List Component', () => {
  rendersWithoutCrashing(MediaListWithItems);
  matchesThePreviousSnapshot(MediaListWithItems, {shallow: true, snapshotName: 'media-list-with-items'});
  matchesThePreviousSnapshot(MediaListFetching, {shallow: true, snapshotName: 'media-list-fetching'});
  matchesThePreviousSnapshot(MediaListNoMatchingItems, {shallow: true, snapshotName: 'media-list-no-matching-items'});
});
