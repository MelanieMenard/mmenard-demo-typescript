import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Breadcrumb } from 'AppSrc/core/components/breadcrumb/breadcrumb';
import { LocationMediaListLazyLoad } from 'AppSrc/core/components/location-media-list-lazyload/location-media-list-lazyload';
import './location-detail.css';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { AnyAction } from 'redux';
import { RouteComponentProps } from 'react-router';
import { BreadcrumbInterface } from 'AppSrc/core/interfaces/Breadcrumb';
import { Location } from 'AppSrc/core/interfaces/Location';
import { makeGetTagById } from 'AppSrc/core/selectors/selectors-tags';


const locationDetailScreenBreadcrumbs: BreadcrumbInterface[] = [
  {
    link: '/',
    title: 'Location map',
  },
];

interface  LocationDetailDisplayProps {
  match: RouteComponentProps<{ locationId: string }>;
  locationId: string;
  location: Location;
}

const LocationDetailDisplay = React.memo<LocationDetailDisplayProps>(({ match, location }) => (
  <div className="location-detail">
    <Breadcrumb
      breadcrumbs={locationDetailScreenBreadcrumbs}
    />
    <h2 className="location-detail-title">{location.displayName}</h2>
    <p className="location-detail-description">{location.description}</p>
    <LocationMediaListLazyLoad
      locationId={location.id}
    />
  </div>
));


/* --- Location detail container component --- */

// MM: 'makeMapStateToProps' is so each instance of the container gets its own mapStateToProps
// otherwise the selectors that depend on props don't properly memoize
// https://redux.js.org/recipes/computing-derived-data#sharing-selectors-across-multiple-components
const makeMapStateToProps = () => {

  const getTagById = makeGetTagById();

  const mapStateToProps = (state: AppState, ownProps: RouteComponentProps<{ locationId: string }>) => ({
    location: getTagById(state, ownProps.match.params.locationId),
  });
  return mapStateToProps;
};

const LocationDetail = withRouter(connect(
  makeMapStateToProps,
)(LocationDetailDisplay));


export {
  LocationDetailDisplay,
  LocationDetail,
};
