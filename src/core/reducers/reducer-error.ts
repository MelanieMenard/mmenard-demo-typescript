/* ------------------------------------------- */
/*   Error Reducer: centralises errors from all actions, so they can be displayed in a snackbar on top level component
/* ------------------------------------------- */

import { AnyAction } from 'redux';
import {
  ErrorState,
  FormattedError,
} from 'AppSrc/core/interfaces/Error';
import { REMOVE_ERROR_DISPLAYED } from 'AppSrc/core/actions/actions-error';

const errorDefaultState: ErrorState = {
  errors: [],
};


const errorReducer = (state: ErrorState = errorDefaultState, action: AnyAction): ErrorState => {

  // MM: to avoid importing all the actions, we catch any action whose type ends in _ERROR
  // IMPORTANT! we rely on consistent action type format in our app, and a formatted error being on the payload!
  if (action.type && action.type.match(/_ERROR$/)) {

    // add the new error only if there isn't already an error with same code in array
    // !!! test error.code not undefined rather than truthy as 0 is valid!!!
    if (action.payload.error && (action.payload.error.code !== undefined) && !state.errors.find((error) => error.code === action.payload.error.code)) {

      return {
        ...state,
        errors: [
          ...state.errors,
          action.payload.error,
        ],
      };
    }
    return state;
  }

  /* remove the first error from array after it has been displayed */
  // MM: bad English grammar because the action type MUST NOT END in '_ERROR' for the reducer not to mistake it for a new error!
  if (action.type === REMOVE_ERROR_DISPLAYED) {
    return {
      ...state,
      errors: [
        ...state.errors.slice(1),
      ],
    };
  }

  /* - Any other action do not affect this reducer, return existing state - */
  return state;
};

export {
  errorDefaultState,
  errorReducer,
};
