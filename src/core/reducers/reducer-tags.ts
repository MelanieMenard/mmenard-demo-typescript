/* ------------------------------------------- */
/*   Tags Reducer
/*   Responsible for the slice of state containing tags
/*   This data is hardcoded (it would come from a custom API in a real app)
/*   So I make it normalised to optimise the state shape
/* ------------------------------------------- */

import { AnyAction } from 'redux';
import { TagsState } from 'AppSrc/core/interfaces/Location';
import {
  SET_SELECTED_TAG,
  SET_TAG_POPULARITY,
} from 'AppSrc/core/actions/actions-tags';
import { FETCH_MEDIA_LIST_SUCCESS } from 'AppSrc/core/actions/actions-media-list';
// MM: if I had my own own custom API, this would come from an endpoint, most likely graphQL, as it contains all the base app config at once
import AppConfig from 'AppSrc/core/data/app-config.json';

const initDefaultStateFromJson = (): TagsState => {

  const defaultState: TagsState = {
    // whether a tag is selected to filter the media on
    selectedTag: null,
    // lookup object of location tags by Id
    locationsById: {},
    // array of all location tag Ids
    allLocations: [],
    // map location types (=thematic tags) to colors
    colorMap: {},
  };

  if (AppConfig.colorMap) {
    defaultState.colorMap = AppConfig.colorMap;
  }

  if (AppConfig.locations && AppConfig.locations.length) {
    AppConfig.locations.forEach((location) => {
      defaultState.locationsById[location.id] = {
        ...location,
        matchingItems: 0,
        popularity: 1,
        popularityWideScale: 1,
      };
      defaultState.allLocations.push(location.id);
    });
  }

  return defaultState;
};

const tagsDefaultState = initDefaultStateFromJson();


const tagsReducer = (state: TagsState = tagsDefaultState, action: AnyAction): TagsState => {

  switch (action.type) {

    case SET_SELECTED_TAG:

      // either set the selected tag, and unselect it if user clicked an already selected tag
      return {
        ...state,
        selectedTag: (state.selectedTag !== action.payload.tagId) ? action.payload.tagId : null,
      };


    /* - A fetch request successfully returned data - */
    case FETCH_MEDIA_LIST_SUCCESS:

      // how many matches there are on the server
      return {
        ...state,
        locationsById: {
          ...state.locationsById,
          [action.payload.tagId]: {
            ...state.locationsById[action.payload.tagId],
            matchingItems: parseInt(action.payload.totalItems),
          },
        },
      };


    case SET_TAG_POPULARITY:

      return {
        ...state,
        locationsById: {
          ...state.locationsById,
          [action.payload.tagId]: {
            ...state.locationsById[action.payload.tagId],
            popularity: action.payload.tagPopularity,
            popularityWideScale: action.payload.tagPopularityWideScale,
          },
        },
      };


    /* - Any other action do not affect this reducer, return existing state - */
    default:
      return state;
  }
};

export {
  tagsDefaultState,
  tagsReducer,
};
