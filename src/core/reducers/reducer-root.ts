/* ------------------------------------------- */
/*   Redux Root Reducer
/*   The Root reducer combines the different reducers responsible for updating specific bits of data in the state
/* ------------------------------------------- */

import { combineReducers } from 'redux';
import { errorReducer } from 'AppSrc/core/reducers/reducer-error';
import { tagsReducer } from 'AppSrc/core/reducers/reducer-tags';
import { mediaListReducer } from 'AppSrc/core/reducers/reducer-media-list';
import { mediaDetailReducer } from 'AppSrc/core/reducers/reducer-media-detail';

const rootReducer = combineReducers({
  error: errorReducer,
  tags: tagsReducer,
  mediaList: mediaListReducer,
  mediaDetail: mediaDetailReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export { rootReducer };
