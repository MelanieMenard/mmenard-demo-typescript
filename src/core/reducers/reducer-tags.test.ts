import {
  tagsDefaultState,
  tagsReducer,
} from 'AppSrc/core/reducers/reducer-tags';
import {
  SET_SELECTED_TAG,
  SET_TAG_POPULARITY,
} from 'AppSrc/core/actions/actions-tags';
import { FETCH_MEDIA_LIST_SUCCESS } from 'AppSrc/core/actions/actions-media-list';
import { 
  expectStateToHaveChanges,
  testDefaultReducerBehaviour,
} from 'AppSrc/core/test/utils/reducer-test-utils';
import {
  mediaListPage1,
  TOTAL_MEDIA_ITEMS_ON_SERVER,
  TOTAL_MEDIA_PAGES_ON_SERVER,
  MEDIA_PER_PAGE_FIXTURE,
} from 'AppSrc/core/test/fixtures/media-list-fixtures';


const testSetSelectedTag = () => {

  describe('it sets the selected tag when it\'s not already selected', () => {
    const action = {
      type: SET_SELECTED_TAG,
      payload: {
        tagId: 'monksHouse',
      }
    };
    const outputState = tagsReducer(tagsDefaultState, action);
    expectStateToHaveChanges(
      outputState,
      tagsDefaultState,
      {
        selectedTag: 'monksHouse'
      }
    );
  });

  describe('it clears the selected tag if it\'s already selected', () => {
    const action = {
      type: SET_SELECTED_TAG,
      payload: {
        tagId: 'monksHouse',
      }
    };
    const initialState = {
      ...tagsDefaultState,
      selectedTag: 'monksHouse',
    };
    const outputState = tagsReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      tagsDefaultState,
      {
        selectedTag: null,
      }
    );
  });
}

const testSetTagPopularity = () => {

  describe('it sets the tag popularity', () => {

    const action = {
      type: SET_TAG_POPULARITY,
      payload: {
        tagId: 'monksHouse',
        tagPopularity: 3,
        tagPopularityWideScale: 12,
      }
    };
    const outputState = tagsReducer(tagsDefaultState, action);
    expectStateToHaveChanges(
      outputState,
      tagsDefaultState,
      {
        locationsById: {
          ...tagsDefaultState.locationsById,
          ['monksHouse']: {
            ...tagsDefaultState.locationsById['monksHouse'],
            popularity: 3,
            popularityWideScale: 12,
          },
        },
      }
    );
  });
}

const testfetchMediaListSuccess = () => {

  describe('it sets the matching item count on fetch success', () => {

    const action = {
      type: FETCH_MEDIA_LIST_SUCCESS,
      payload: {
        tagId: 'monksHouse',
        items: mediaListPage1,
        totalItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
        currentPage: 1,
        totalPages: TOTAL_MEDIA_PAGES_ON_SERVER,
        perPage: MEDIA_PER_PAGE_FIXTURE,
      }
    };
    const outputState = tagsReducer(tagsDefaultState, action);
    expectStateToHaveChanges(
      outputState,
      tagsDefaultState,
      {
        locationsById: {
          ...tagsDefaultState.locationsById,
          ['monksHouse']: {
            ...tagsDefaultState.locationsById['monksHouse'],
            matchingItems: TOTAL_MEDIA_ITEMS_ON_SERVER,
          },
        },
      }
    );
  });
}


describe('tagsReducer', () => {
  testDefaultReducerBehaviour(tagsReducer, tagsDefaultState);
  testSetSelectedTag();
  testfetchMediaListSuccess();
  testSetTagPopularity();
});
