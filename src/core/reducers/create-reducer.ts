/** Utility function to generate a reducer based on an object mapping action types to handler functions * */
/* Used instead of a switch case when each case would be too complex
/* (typically requires const declarations, which eslint forbids in case branches)
/*  Based on:
/*  https://redux.js.org/recipes/reducing-boilerplate#generating-reducers
 * */

import { AnyAction } from 'redux';

// Typescript indexable types: https://www.typescriptlang.org/docs/handbook/interfaces.html#indexable-types
// a string indexed lookup table of reducer functions
export interface ReducerFunctionsByActionNames<StateType> {
  [index: string]: (state: StateType, action: AnyAction) => StateType;
}

// defaultState: the defined default state used to initalise the slice of state
// handlers: An object whose keys are the actions types to respond to, and whose values are functions that return the state based on those actions.
// Use a generic <StateType> so that the typescript can infer the type of state returned by the reducer
// For generics see https://www.typescriptlang.org/docs/handbook/generics.html
const createReducer = <StateType>(defaultState: StateType, handlers: ReducerFunctionsByActionNames<StateType>) => {
  // return a reducer function based on the parameters
  return (state: StateType = defaultState, action: AnyAction) => {
    // if the current action is contained as a key in the incoming handlers object
    if (handlers.hasOwnProperty(action.type)) {
      // pass the state an action to the handler function
      return handlers[action.type](state, action);
    }
    // if no handler has the action.type as key, return the state untouched
    return state;
  };
};

export { createReducer };
