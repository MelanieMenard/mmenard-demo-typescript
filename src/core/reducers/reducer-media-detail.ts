/* ------------------------------------------- */
/*   Media Detail Reducer
/*   Responsible for the slice of state containing media items retrieved from API
/* ------------------------------------------- */

import { AnyAction } from 'redux';
import { MediaDetailState } from 'AppSrc/core/interfaces/Media';
import {
  FETCH_MEDIA_DETAIL_REQUEST,
  FETCH_MEDIA_DETAIL_SUCCESS,
  FETCH_MEDIA_DETAIL_ERROR,
} from 'AppSrc/core/actions/actions-media-detail';

const mediaDetailReducerDefaultState: MediaDetailState = {
  isFetching: false,
  // media detail info by Id
  mediaById: {},
};

const mediaDetailReducer = (state: MediaDetailState = mediaDetailReducerDefaultState, action: AnyAction): MediaDetailState => {

  switch (action.type) {


    /* - A fetch request is being sent to the API - */
    case FETCH_MEDIA_DETAIL_REQUEST:

      return {
        ...state,
        isFetching: true,
      };


    /* - A fetch request successfully returned data - */
    case FETCH_MEDIA_DETAIL_SUCCESS:

      // append new items to end of existing array
      return {
        ...state,
        isFetching: false,
        mediaById: {
          ...state.mediaById,
          [action.payload.mediaId]: action.payload.mediaData,
        },
      };


    /* - A fetch request resulted in an error - */
    // in a real app, the error handling proper happens in the query or thunk action, the reducer only needs to show it's no longer fetching
    case FETCH_MEDIA_DETAIL_ERROR:

      return {
        ...state,
        isFetching: false,
      };


    /* - Any other action do not affect this reducer, return existing state - */
    default:
      return state;
  }
};

export {
  mediaDetailReducerDefaultState,
  mediaDetailReducer,
};
