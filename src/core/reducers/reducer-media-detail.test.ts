import {
  mediaDetailReducerDefaultState,
  mediaDetailReducer,
} from 'AppSrc/core/reducers/reducer-media-detail';
import {
  FETCH_MEDIA_DETAIL_REQUEST,
  FETCH_MEDIA_DETAIL_SUCCESS,
  FETCH_MEDIA_DETAIL_ERROR,
} from 'AppSrc/core/actions/actions-media-detail';
import { 
  expectStateToHaveChanges,
  testDefaultReducerBehaviour,
} from 'AppSrc/core/test/utils/reducer-test-utils';
import { testMediaDetail } from 'AppSrc/core/test/fixtures/media-detail-fixtures';
import { testError } from 'AppSrc/core/test/fixtures/error-fixture';


const testfetchMediaDetailRequest = () => {

  describe('it sets isFetching to true on fetch request', () => {

    const action = {
      type: FETCH_MEDIA_DETAIL_REQUEST,
      payload: {
        mediaId: testMediaDetail.id,
      }
    };
    const outputState = mediaDetailReducer(mediaDetailReducerDefaultState, action);
    expectStateToHaveChanges(
      outputState,
      mediaDetailReducerDefaultState,
      {
        isFetching: true,
      }
    );
  });
}

const testfetchMediaDetailError = () => {

  describe('it sets isFetching to false on fetch error', () => {

    const initialState = {
      ...mediaDetailReducerDefaultState,
      isFetching: true,
    };
    const action = {
      type: FETCH_MEDIA_DETAIL_ERROR,
      payload: {
        mediaId: testMediaDetail.id,
        error: testError,
      }
    };
    const outputState = mediaDetailReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        isFetching: false,
      }
    );
  });
}

const testfetchMediaDetailSuccess = () => {

  describe('it stores mediaData using Id as key on fetch success', () => {

    const action = {
      type: FETCH_MEDIA_DETAIL_SUCCESS,
      payload: {
        mediaId: testMediaDetail.id,
        mediaData: testMediaDetail,
      }
    };
    const initialState = {
      ...mediaDetailReducerDefaultState,
      isFetching: true,
    };
    const outputState = mediaDetailReducer(initialState, action);
    expectStateToHaveChanges(
      outputState,
      initialState,
      {
        isFetching: false,
        mediaById: {
          ...initialState.mediaById,
          [action.payload.mediaId]: action.payload.mediaData,
        },
      }
    );
  });
}


describe('mediaDetailReducer', () => {
  testDefaultReducerBehaviour(mediaDetailReducer, mediaDetailReducerDefaultState);
  testfetchMediaDetailRequest();
  testfetchMediaDetailError();
  testfetchMediaDetailSuccess();
});
