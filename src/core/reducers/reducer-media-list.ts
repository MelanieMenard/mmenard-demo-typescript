/* ------------------------------------------- */
/*   Media List Reducer
/*   Responsible for the slice of state containing media items retrieved from API
/* ------------------------------------------- */

import { AnyAction } from 'redux';
import { MediaListState } from 'AppSrc/core/interfaces/Media';
import { createReducer } from 'AppSrc/core/reducers/create-reducer';
import {
  FETCH_MEDIA_LIST_REQUEST,
  FETCH_MEDIA_LIST_SUCCESS,
  FETCH_MEDIA_LIST_ERROR,
} from 'AppSrc/core/actions/actions-media-list';
import { paginatedArray } from 'AppSrc/core/utilities/pagination-utils';

// MM: if I had my own own custom API, this would come from an endpoint, most likely graphQL, as it contains all the base app config at once
import AppConfig from 'AppSrc/core/data/app-config.json';

const initDefaultStateFromJson = (): MediaListState => {

  const defaultState: MediaListState = {
    // media items from API, and their fetching status
    mediaByLocation: {},
  };

  if (AppConfig.locations && AppConfig.locations.length) {
    AppConfig.locations.forEach((location) => {
      defaultState.mediaByLocation[location.id] = {
        isFetched: false,
        isFetching: false,
        total: 0,
        currentPage: 0,
        totalPages: 0,
        perPage: 0,
        items: [],
      };
    });
  }

  return defaultState;
};

const mediaListReducerDefaultState: MediaListState = initDefaultStateFromJson();


/* - A fetch request is being sent to the API - */
const fetchMediaListRequest = (state: MediaListState, action: AnyAction): MediaListState => ({
  ...state,
  mediaByLocation: {
    ...state.mediaByLocation,
    [action.payload.tagId]: {
      ...state.mediaByLocation[action.payload.tagId],
      isFetching: true,
    },
  },
});


/* - A fetch request resulted in an error - */
// the error handling proper happens in the thunk action, the reducer only needs to show it's no longer fetching
const fetchMediaListError = (state: MediaListState, action: AnyAction): MediaListState => ({
  ...state,
  mediaByLocation: {
    ...state.mediaByLocation,
    [action.payload.tagId]: {
      ...state.mediaByLocation[action.payload.tagId],
      isFetching: false,
    },
  },
});


/* - A fetch request successfully returned data - */
const fetchMediaListSuccess = (state: MediaListState, action: AnyAction): MediaListState => {

  // concat the newly fetched page with the existing items
  const existingItems = state.mediaByLocation[action.payload.tagId].items;
  const fetchedItems = action.payload.items;
  const totalItemsCount = action.payload.totalItems;
  const currentPage = action.payload.currentPage;
  const perPage = action.payload.perPage;
  const startIndex = (currentPage - 1) * perPage;

  const processedData = paginatedArray(existingItems, fetchedItems, totalItemsCount, startIndex, perPage);
  const { paginatedItems } = processedData;

  // append new items to end of existing array
  // isFetched needed separately from items.length for requests that are made but return no result
  return {
    ...state,
    mediaByLocation: {
      ...state.mediaByLocation,
      [action.payload.tagId]: {
        ...state.mediaByLocation[action.payload.tagId],
        isFetched: true,
        isFetching: false,
        total: action.payload.totalItems,
        currentPage: action.payload.currentPage,
        totalPages: action.payload.totalPages,
        perPage: action.payload.perPage,
        items: [
          ...paginatedItems,
        ],
      },
    },
  };

};


// Action handlers dictionary
// defines the mapping between actions and the handler methods
const actionHandlers = {
  [FETCH_MEDIA_LIST_REQUEST]: fetchMediaListRequest,
  [FETCH_MEDIA_LIST_SUCCESS]: fetchMediaListSuccess,
  [FETCH_MEDIA_LIST_ERROR]: fetchMediaListError,
};

// create the reducer
const mediaListReducer = createReducer <MediaListState>(mediaListReducerDefaultState, actionHandlers);

export {
  mediaListReducerDefaultState,
  mediaListReducer,
};
