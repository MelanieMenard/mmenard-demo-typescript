import {
  removeDisplayedError,
  REMOVE_ERROR_DISPLAYED,
} from 'AppSrc/core/actions/actions-error';
import { testSynchronousActionCreator } from 'AppSrc/core/test/utils/action-test-utils';

const testErrorSynchronousActions = () => {

  describe('Error synchronous action creators', () => {

    testSynchronousActionCreator(
      removeDisplayedError,
      REMOVE_ERROR_DISPLAYED,
      {},
    );
  });
}

// run the actual tests
describe('Error actions', () => {
  testErrorSynchronousActions();
});