/* ------------------------------------------- */
/*   Actions to clear errors after they've been shown in a snackbar
/* ------------------------------------------- */

import { Action, ActionCreator } from 'redux';

/* --- Actions Types --- */

// MM: bad English grammar because the action type MUST NOT END in '_ERROR' for the reducer not to mistake it for a new error!
const REMOVE_ERROR_DISPLAYED = 'REMOVE_ERROR_DISPLAYED';


/* ------ ACTION CREATORS ------ */

const removeDisplayedError: ActionCreator<Action> = () => ({
  type: REMOVE_ERROR_DISPLAYED,
  payload: {},
});


export {
  removeDisplayedError,
  REMOVE_ERROR_DISPLAYED,
};
