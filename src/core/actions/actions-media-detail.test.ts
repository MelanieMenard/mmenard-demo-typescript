import {
	fetchMediaDetailRequest,
  fetchMediaDetailSuccess,
  fetchMediaDetailError,
  fetchMediaDetail,
  FETCH_MEDIA_DETAIL_REQUEST,
  FETCH_MEDIA_DETAIL_SUCCESS,
  FETCH_MEDIA_DETAIL_ERROR
} from 'AppSrc/core/actions/actions-media-detail';
import {  
  mockStore,
  testSynchronousActionCreator,
} from 'AppSrc/core/test/utils/action-test-utils';
import { testError } from 'AppSrc/core/test/fixtures/error-fixture';
import { testMediaDetail } from 'AppSrc/core/test/fixtures/media-detail-fixtures';
import { getFlickrPhotoInfoQuery } from 'AppSrc/core/queries/axios-queries';
import { mediaDetailReducerDefaultState } from 'AppSrc/core/reducers/reducer-media-detail';


const testMediaDetailSynchronousActions = () => {

  describe('media detail synchronous action creators', () => {

    testSynchronousActionCreator(
      fetchMediaDetailRequest,
      FETCH_MEDIA_DETAIL_REQUEST,
      { mediaId: testMediaDetail.id }
    );

    testSynchronousActionCreator(
      fetchMediaDetailSuccess,
      FETCH_MEDIA_DETAIL_SUCCESS,
      {
        mediaId: testMediaDetail.id,
        mediaData: testMediaDetail,
      }
    );

    testSynchronousActionCreator(
      fetchMediaDetailError,
      FETCH_MEDIA_DETAIL_ERROR,
      {
        mediaId: testMediaDetail.id,
        error: testError,
      }
    );
  });
}

// Mock the axios query imported in the async action
jest.mock('AppSrc/core/queries/axios-queries', () => ({
  getFlickrPhotoInfoQuery: jest.fn(() => true),
}));


const testMediaDetailAsyncThunkActions = () => {

  describe('media detail async thunk actions', () => {

    it('fetchMediaDetail dispatches request and success', () => {

      const store = mockStore({ mediaDetail: mediaDetailReducerDefaultState });

      // mock a standard media detail as return value from the API
      getFlickrPhotoInfoQuery.mockResolvedValue(testMediaDetail);

      // expect.assertions(expectedAssertionsCount) tells Jest how many assertations should be tested in async code
      expect.assertions(2);

      // dispatch the async action into the mock store
      return store.dispatch(fetchMediaDetail(testMediaDetail.id)).then(() => {

        // expect the query to have been called with the tag's search string
        expect(getFlickrPhotoInfoQuery).toHaveBeenCalledWith(testMediaDetail.id);
   
        const expectedActions = [
          { 
            type: 'FETCH_MEDIA_DETAIL_REQUEST',
            payload: {
              mediaId: testMediaDetail.id,
            }
          },
          {
            type: 'FETCH_MEDIA_DETAIL_SUCCESS',
            payload: {
              mediaId: testMediaDetail.id,
              mediaData: testMediaDetail,
            }
          }
        ];

        // expect the request and success actions to have been dispatched
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });   
    });

    it('fetchMediaDetail dispatches request and error', () => {

      const store = mockStore({ mediaDetail: mediaDetailReducerDefaultState });

      // mock a standard error as return value from the API
      getFlickrPhotoInfoQuery.mockRejectedValue(testError);

      expect.assertions(2);

      // dispatch the async action into the mock store
      return store.dispatch(fetchMediaDetail(testMediaDetail.id)).then(() => {

        // expect the query to have been called with the tag's search string
        expect(getFlickrPhotoInfoQuery).toHaveBeenCalledWith(testMediaDetail.id);

        const expectedActions = [
          { 
            type: 'FETCH_MEDIA_DETAIL_REQUEST',
            payload: {
              mediaId: testMediaDetail.id,
            }
          },
          {
            type: 'FETCH_MEDIA_DETAIL_ERROR',
            payload: {
              mediaId: testMediaDetail.id,
              error: testError,
            }
          }
        ];

        // expect the request and success actions to have been dispatched
        const actions = store.getActions();
        expect(actions).toEqual(expectedActions);
      });   
    });
  });

}

// run the actual tests
describe('media detail actions', () => {
  testMediaDetailSynchronousActions();
  testMediaDetailAsyncThunkActions();
});
