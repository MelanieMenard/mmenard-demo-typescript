import {
	setSelectedTag,
  SET_SELECTED_TAG,
  setTagPopularity,
  SET_TAG_POPULARITY,
} from 'AppSrc/core/actions/actions-tags';
import { testSynchronousActionCreator } from 'AppSrc/core/test/utils/action-test-utils';


const testTagSynchronousActions = () => {
  describe('tags synchronous action creators', () => {

    testSynchronousActionCreator(
      setSelectedTag,
      SET_SELECTED_TAG,
      {tagId: 'tagId1' }
    );

    testSynchronousActionCreator(
      setTagPopularity,
      SET_TAG_POPULARITY,
      {
        tagId: 'tagId1',
        tagPopularity: 3,
        tagPopularityWideScale: 12,
      },
    );
  });
}

// run the actual tests
describe('tag actions', () => {
  testTagSynchronousActions();
});
