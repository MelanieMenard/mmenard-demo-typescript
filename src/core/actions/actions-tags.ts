/* ------------------------------------------- */
/*   Actions for the tags
/*   Described what happened in the UI/App Logic to Redux
/*   Then the reducers decide whether the action should result in a state update
/*   No async/API for the tags so only simple synchronous actions
/*   Follow the Flux Standard Action format that wraps the properties inside 'payload' object, as I find it neater
/*   https://github.com/redux-utilities/flux-standard-action
/* ------------------------------------------- */

// tslint doesn't like the legit syntax to import only parts of d3
/* tslint:disable:no-implicit-dependencies*/
import { scaleLog } from 'd3-scale';
import {
  Location,
  TagsState,
} from 'AppSrc/core/interfaces/Location';
import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { Action, ActionCreator, AnyAction } from 'redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';


/* --- Actions Types --- */
// Defining actions types as constants rather than strings make the error picked up earlier if you try to dispatch an action that does not exist
// it throws an undefined constant error immediately instead of dispatching a non existing action through the system that just flows though without triggering any reducer.

const SET_SELECTED_TAG = 'SET_SELECTED_TAG';
const SET_TAG_POPULARITY = 'SET_TAG_POPULARITY';


/* ------ ACTION CREATORS ------ */

/* - set tag synchronous actions - */

const setSelectedTag: ActionCreator<Action> = (tagId: string) => ({
  type: SET_SELECTED_TAG,
  payload: {
    tagId,
  },
});

const setTagPopularity: ActionCreator<Action> = (tagId: string, tagPopularity: number, tagPopularityWideScale: number) => ({
  type: SET_TAG_POPULARITY,
  payload: {
    tagId,
    tagPopularity,
    tagPopularityWideScale,
  },
});


// utility function to find the min and max of matchingItems for all tags, and make d3 scale from it
const calculateTagsPopularityScale = (tags: TagsState) => {
  const tagResultCountArray = tags.allLocations.map((tagId) => tags.locationsById[tagId].matchingItems);
  // sort integers
  tagResultCountArray.sort((a, b) => a - b);
  // prevent 0 value on Log scale
  const resultsMin = tagResultCountArray[0] || 1;
  const resultsMax = tagResultCountArray[tagResultCountArray.length - 1] || 1;
  const popularityScale = scaleLog();
  popularityScale.domain([resultsMin, resultsMax]).range([1, 6]);
  const popularityScaleWider = scaleLog();
  popularityScaleWider.domain([resultsMin, resultsMax]).range([3, 25]);
  return { popularityScale, popularityScaleWider };
};


/* - Calculate relative popularity for all locations in the tag list: 'thunk' action that dispatches other thunk actions  - */
const calculateTagsPopularity = (): ThunkAction<void, AppState, void, AnyAction> =>
  (dispatch, getState) => {

    const tags = getState().tags;

    // find the min and max of matchingItems for all tags, and make d3 scale from it
    const { popularityScale, popularityScaleWider } = calculateTagsPopularityScale(tags);

    tags.allLocations.forEach((tagId) => {
      const matchingItems = tags.locationsById[tagId].matchingItems;
      // leave default value of 1 if no matching items!
      if (matchingItems > 0) {
        const tagPopularity = Math.round(popularityScale(matchingItems));
        const tagPopularityWideScale = Math.round(popularityScaleWider(matchingItems));
        dispatch(setTagPopularity(tagId, tagPopularity, tagPopularityWideScale));
      }
    });
  };


export {
  setSelectedTag,
  SET_SELECTED_TAG,
  setTagPopularity,
  SET_TAG_POPULARITY,
  calculateTagsPopularity,
};
