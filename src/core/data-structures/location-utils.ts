/** Utility function that finds the location a media item is related to */

import { AppState } from 'AppSrc/core/reducers/reducer-root';
import { Location } from 'AppSrc/core/interfaces/Location';

const findMediaParentLocation = (state: AppState, mediaId: string): Location | null => {

  const locationsById = state.tags.locationsById;
  const mediaByLocation = state.mediaList.mediaByLocation;
  const locationIds = Object.keys(mediaByLocation);

  const parentLocationId = locationIds.find((locationId) => mediaByLocation[locationId].items.some((media) => (media.id === mediaId)));
  const parentLocation = parentLocationId ? locationsById[parentLocationId] : null;

  return parentLocation;
};

export { findMediaParentLocation };
