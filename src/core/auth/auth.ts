/* ------------------------------------------- */
/*   Auth JS file
/*   Currently just has an object with the App Credentials
/*   but that's where all the token business logic would go
/* ------------------------------------------- */

const AppCredentials = {
  Flickr: {
    key: process.env.REACT_APP_FLICKR_KEY,
    secret: process.env.REACT_APP_FLICKR_SECRET,
  },
};

export { AppCredentials };
