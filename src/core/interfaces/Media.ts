export interface MediaDetailInterface {
  id: string;
  secret: string;
  link: string;
  title: string;
  description: string;
  author: string;
  dateTaken: string;
}

export interface MediaItem {
  id: string;
  title: string;
  secret: string;
  authorId: string;
  image: string;
}

export interface MediaDetailById {
  [index: string]: MediaDetailInterface;
}

// Media Detail Reducer interface.
export interface MediaDetailState {
  isFetching: boolean;
  mediaById: MediaDetailById;
}

export interface PaginatedMediaListForLocation {
  isFetched: boolean;
  isFetching: boolean;
  total: number;
  currentPage: number;
  totalPages: number;
  perPage: number;
  items: MediaItem[];
}

export interface MediaListByLocation {
  [index: string]: PaginatedMediaListForLocation;
}

// Media List Reducer interface.
export interface MediaListState {
  mediaByLocation: MediaListByLocation;
}

export interface FlickrPhotoSearchResponse {
  total: number;
  pages: number;
  page: number;
  perPage: number;
  items: MediaItem[];
}
