export interface BreadcrumbInterface {
  link: string;
  title: string;
}
