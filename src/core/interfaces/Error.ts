// consistently formatted error made by the error handler
// then we reject the query Promise with this formatted error for the reducer to catch. the reducer expect an error in this consistent format only

export interface FormattedError {
  code: number;
  message: string;
}

// Error Reducer interface.
export interface ErrorState {
  errors: FormattedError[];
}