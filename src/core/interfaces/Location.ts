export interface Location {
  id: string;
  displayName: string;
  searchQuery: string;
  description: string,
  type: string;
  latitude: number;
  longitude: number;
  matchingItems: number;
  popularity: number;
  popularityWideScale: number;
}

// Typescript indexable types: https://www.typescriptlang.org/docs/handbook/interfaces.html#indexable-types
export interface ColorMap {
  [index: string]: string;
}

export interface LocationsById {
  [index: string]: Location;
}

// Tags Reducer interface.
export interface TagsState {
  selectedTag: string | null;
  locationsById: LocationsById;
  allLocations: string[];
  colorMap: ColorMap;
}
