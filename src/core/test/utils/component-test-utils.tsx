/****************************************************
  Utility function to test React components
/*****************************************************/

import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router';
import { shallow, mount } from 'enzyme';

// TestRouter to wrap components that contain router components such as <Link />
// this will provide a router in the context
// see https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/testing.md#context
// The memory router creates random history entry keys, which breaks snapshot tests as they
// key would change on each run. We prevent this by passing in an initial test route
// see https://github.com/ReactTraining/react-router/issues/5579
const TestRouter: React.FunctionComponent = (props) => (
  <MemoryRouter
    initialEntries={[{ pathname: '/', key: 'testKey' }]}
  >
    { props.children }
  </MemoryRouter>
)


// check that the wrapped component renders without crashing
const rendersWithoutCrashing = (component: JSX.Element, promise?: Promise<any>): void => {
  it('renders and unmounts without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(component, div);
    if (promise) {
      // dont unmount component unless the promise has resolved
      promise.then(() => ReactDOM.unmountComponentAtNode(div));
    } else {
      ReactDOM.unmountComponentAtNode(div);
    }
  }); 
}

interface SnapshotOptions {
  snapshotName?: string;
  shallow?: boolean;
}

// check that rendered component matches previous snapshot
// paramteres
// component: the component to render as a snapshot
// options: (optional)
//    snapshotName: the name of the shapshot. defaults to ''
//    shallow: boolean indicating whether the snapshot should be a shallow render
const matchesThePreviousSnapshot = (component: JSX.Element, options?: SnapshotOptions): void => {
  const snapshotName = options && options.snapshotName;
  test('matches the previous snapshot', ()=> {
    const useShallow = options && options.shallow;
    const wrap = useShallow ? shallow(component) : mount(component);
    expect(wrap).toMatchSnapshot(snapshotName);
  })
}

export { TestRouter, rendersWithoutCrashing, matchesThePreviousSnapshot }