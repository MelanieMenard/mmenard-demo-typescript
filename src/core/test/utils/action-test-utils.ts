/****************************************************
  Utility function to test Redux actions
/*****************************************************/

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { Action, ActionCreator, AnyAction } from 'redux';

// set up a mock store to test async actions
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

// very simple test to test a synchronous action creator creates an action with the correct payload and type
const testSynchronousActionCreator = (actionCreator: ActionCreator<Action>, expectedType: string, expectedPayload: any) => {

  it('creates an action with the correct type and payload', () => {
    const expectedAction: AnyAction = {
      type: expectedType,
      payload: expectedPayload,
    }
    expect(actionCreator(...Object.values(expectedPayload))).toEqual(expectedAction);
  })

}


export {
  mockStore,
  testSynchronousActionCreator,
};
