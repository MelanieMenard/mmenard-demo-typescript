const tag1 = {
  id: 'tagId1',
  displayName: 'tag1 displayName',
  searchQuery: 'tag1 searchQuery',
  description: 'tag1 description',
  type: 'tag1 type',
  latitude: 50.8387,
  longitude: 0.0165,
  popularity: 1,
  popularityWideScale: 1,
};

const tag2 = {
  id: 'tagId2',
  displayName: 'tag2 displayName',
  searchQuery: 'tag2 searchQuery',
  description: 'tag2 description',
  type: 'tag2 type',
  latitude: 50.8387,
  longitude: 0.0165,
  popularity: 1,
  popularityWideScale: 1,
};

const tag3 = {
  id: 'tagId3',
  displayName: 'tag3 displayName',
  searchQuery: 'tag3 searchQuery',
  description: 'tag3 description',
  type: 'tag3 type',
  latitude: 50.8387,
  longitude: 0.0165,
  popularity: 1,
  popularityWideScale: 1,
};

const tagIdsArray = [
  'tagId1',
  'tagId2',
  'tagId3', 
];

const tagsArray = [
  tag1,
  tag2,
  tag3,
];

const tagsById = {
  tagId1: tag1,
  tagId2: tag2,
  tagId3: tag3, 
};

const colorMap = {
  tagId1: "#FF0000",
  tagId2: "#00FF00",
  tagId3: "#0000FF", 
};

export {
  tag1,
  tagIdsArray,
  tagsArray,
  tagsById,
  colorMap,
};
