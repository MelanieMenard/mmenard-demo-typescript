const testMediaDetail = {
  id: 'testMediaId',
  secret: 'testMediaSecret',
  link: 'www.link.com/testMedia.jpg',
  title: 'testMedia title',
  description: 'testMedia description',
  author: 'testMedia author',
  dateTaken: 'testMedia dateTaken',
};

export { testMediaDetail };
