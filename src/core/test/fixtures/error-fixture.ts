const testError = {
   code: 0,
   message: 'Unknown HTTP error',
};

const testError1 = {
   code: 1,
   message: 'Error code 1',
};

const testError2 = {
   code: 2,
   message: 'Error code 2',
};

export {
  testError,
  testError1,
  testError2,
};
