/* ------------------------------------------- */
/*   Redux Data Store (only 1 per Redux app)
/* ------------------------------------------- */

import { configureStore } from 'redux-starter-kit';
import { rootReducer } from 'AppSrc/core/reducers/reducer-root';

// https://redux.js.org/recipes/configuring-your-store
// https://redux-starter-kit.js.org/tutorials/basic-tutorial
// configureStore automatically installs redux-devtools-extension and the following middleware:
// - redux-thunk
// In Dev only:
// - redux-immutable-state-invariant: warns about accidental state mutation
// thttps://github.com/leoasis/redux-immutable-state-invariant
// - serializable-state-invariant-middleware: warn if you put in the store anything else than plain JS objects
const store = configureStore({
  reducer: rootReducer,
});

export { store };
