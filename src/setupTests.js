// The path src/setupTests is configured in create-react-app to be run at
// the initialisation of tests. This allows enzyme setup code
import { configure } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });